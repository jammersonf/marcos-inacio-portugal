<?php

use App\Models\Number;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNumbersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('numbers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('partners');
            $table->unsignedInteger('clients');
            $table->unsignedInteger('lawyers');
            $table->unsignedInteger('countries');
            $table->unsignedInteger('people');
            $table->unsignedInteger('experience');
            $table->timestamps();
        });

        Number::create([
            'partners' => 0,
            'clients' => 0,
            'lawyers' => 0,
            'countries' => 0,
            'people' => 0,
            'experience' => 0,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('numbers');
    }
}
