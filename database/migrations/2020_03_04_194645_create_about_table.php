<?php

use App\Models\About;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAboutTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abouts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->text('subtitle');
            $table->text('desc');
            $table->string('img');
            $table->string('lang');
            $table->timestamps();
        });

        About::create([
            'title' => '',
            'subtitle' => '',
            'desc' => '',
            'img' => '',
            'lang' => 'pt',
        ]);

        About::create([
            'title' => '',
            'subtitle' => '',
            'desc' => '',
            'img' => '',
            'lang' => 'en',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('about');
    }
}
