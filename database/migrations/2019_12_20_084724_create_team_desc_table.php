<?php

use App\Models\TeamDesc;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamDescTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team_desc', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('text');
            $table->string('lang');
            $table->timestamps();
        });

        TeamDesc::create(['text' => 'Descrição', 'lang' => 'pt']);
        TeamDesc::create(['text' => 'Descricion', 'lang' => 'en']);

        $user = new User();
        $user->name = 'Henrique Toscano';
        $user->email = 'henriquetoscanoadv@gmail.com';
        $user->password = bcrypt('mia@portugal');
        $user->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team_desc');
    }
}
