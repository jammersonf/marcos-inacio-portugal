<?php

use App\Models\ServiceDesc;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateLangTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('home_imgs', function (Blueprint $table) {
            $table->string('lang')->default('pt');
        });
        Schema::table('services', function (Blueprint $table) {
            $table->string('lang')->default('pt');
        });
        Schema::table('service_desc', function (Blueprint $table) {
            $table->string('lang')->default('pt');
        });
        Schema::table('team', function (Blueprint $table) {
            $table->string('lang')->default('pt');
        });
        Schema::table('posts', function (Blueprint $table) {
            $table->string('lang')->default('pt');
        });
        Schema::table('post_categories', function (Blueprint $table) {
            $table->string('lang')->default('pt');
        });
        Schema::table('testimonials', function (Blueprint $table) {
            $table->string('lang')->default('pt');
        });

        ServiceDesc::create(['desc' => 'Descricion', 'lang' => 'en']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('home_imgs', function (Blueprint $table) {
            $table->dropColumn('lang');
        });
        Schema::table('services', function (Blueprint $table) {
            $table->dropColumn('lang');
        });
        Schema::table('service_desc', function (Blueprint $table) {
            $table->dropColumn('lang');
        });
        Schema::table('team', function (Blueprint $table) {
            $table->dropColumn('lang');
        });
        Schema::table('posts', function (Blueprint $table) {
            $table->dropColumn('lang');
        });
        Schema::table('post_categories', function (Blueprint $table) {
            $table->dropColumn('lang');
        });
        Schema::table('testimonials', function (Blueprint $table) {
            $table->dropColumn('lang');
        });
    }
}
