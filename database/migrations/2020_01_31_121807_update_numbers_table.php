<?php

use App\Models\Number;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateNumbersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('numbers', function (Blueprint $table) {
            $table->string('partnerText')->nullable();
            $table->string('clientText')->nullable();
            $table->string('lawyerText')->nullable();
            $table->string('countryText')->nullable();
            $table->string('peopleText')->nullable();
            $table->string('experienceText')->nullable();
        });

        Number::find(1)->update([
            'partnerText' => 'Título 1',
            'clientText' => 'Título 2',
            'lawyerText' => 'Título 3',
            'countryText' => 'Título 4',
            'peopleText' => 'Título 5',
            'experienceText' => 'Título 6',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('numbers', function (Blueprint $table) {
            $table->dropColumn('partnerText');
            $table->dropColumn('clientText');
            $table->dropColumn('lawyerText');
            $table->dropColumn('countryText');
            $table->dropColumn('peopleText');
            $table->dropColumn('experienceText');
        });
    }
}
