<?php

use App\Models\Number;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateNumbers2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('numbers');

        Schema::create('numbers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('lang');
            $table->string('t1')->nullable();
            $table->string('n1')->nullable();
            $table->string('t2')->nullable();
            $table->string('n2')->nullable();
            $table->string('t3')->nullable();
            $table->string('n3')->nullable();
            $table->string('t4')->nullable();
            $table->string('n4')->nullable();
            $table->string('t5')->nullable();
            $table->string('n5')->nullable();
            $table->string('t6')->nullable();
            $table->string('n6')->nullable();
            $table->timestamps();
        });

        Number::create(['lang' => 'pt']);
        Number::create(['lang' => 'en']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('numbers');
    }
}
