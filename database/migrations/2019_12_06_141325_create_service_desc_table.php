<?php

use App\Models\ServiceDesc;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceDescTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_desc', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('desc');
            $table->timestamps();
        });

        ServiceDesc::create(['desc' => 'Descrição']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_desc');
    }
}
