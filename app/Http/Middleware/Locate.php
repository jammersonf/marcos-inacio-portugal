<?php

namespace App\Http\Middleware;

use Closure;

class Locate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->lang && !session('locale')) {
            session()->put('locale', 'pt');
        }
        if ($request->lang) {
            session()->put('locale', $request->lang);
        }

        app()->setLocale(session('locale'));
        return $next($request);
    }
}
