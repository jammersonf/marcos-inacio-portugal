<?php

namespace App\Http\Controllers;

use App\Models\Team;
use App\Models\TeamDesc;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    protected $team;

    public function __construct(Team $team) {
        $this->middleware('auth');
        $this->team = $team;
    }

    public function index()
    {
        $data = $this->team->all();

        return view('admin.dashboard.team.index', compact('data'));
    }

    public function create()
    {
        return view('admin.dashboard.team.form');
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $imageName = time().'.'.$input['image']->extension();  
        $input['image']->move(public_path('storage/team'), $imageName);

        $input['image'] = $imageName;
        $this->team->create($input);

        return redirect()->route('adm.team.index');
    }

    public function show($id)
    {
        $team = $this->team->find($id);
        $data = $this->team->all();

        return view('admin.dashboard.team.form', compact('team', 'data'));
    }

    public function update($id, Request $request)
    {
        $input = $request->all();
        if ($input['image']) {
            $imageName = time().'.'.$input['image']->extension();  
            $input['image']->move(public_path('storage/team'), $imageName);

            $input['image'] = $imageName;
        } else {
            unset($input['image']);
        }
        $this->team->find($id)->update($input);

        return redirect()->route('adm.team.index');
    }

    public function delete($id)
    {
        $this->team->find($id)->delete();
        
        return redirect()->route('adm.team.index');
    }

    public function createDesc()
    {
        $pt = TeamDesc::find(1);
        $en = TeamDesc::find(2);

        return view('admin.dashboard.team.desc', compact('pt', 'en'));
    }

    public function updateDesc(Request $request)
    {
        $desc = TeamDesc::find(1)->update(['text' => $request->pt]);
        $desc = TeamDesc::find(2)->update(['text' => $request->en]);

        toastr()->success('Salvo');

        return redirect()->route('adm.team.createDesc');
    }
}
