<?php

namespace App\Http\Controllers;

use App\Models\Testimonial;
use Illuminate\Http\Request;

class TestimonyController extends Controller
{
    protected $testimonial;

    public function __construct(Testimonial $s) {
        $this->middleware('auth');
        $this->testimonial = $s;
    }

    public function index()
    {
        $data = $this->testimonial->all();

        return view('admin.dashboard.testimonial.index', compact('data'));
    }

    public function create()
    {
        return view('admin.dashboard.testimonial.form');
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $this->testimonial->create($input);

        return redirect()->route('adm.testimonial.index');
    }

    public function show($id)
    {
        $testimonial = $this->testimonial->find($id);

        return view('admin.dashboard.testimonial.form', compact('testimonial'));
    }

    public function update($id, Request $request)
    {
        $input = $request->all();
        $this->testimonial->find($id)->update($input);

        return redirect()->route('adm.testimonial.index');
    }

    public function delete($id)
    {
        $this->testimonial->find($id)->delete();
        
        return redirect()->route('adm.testimonial.index');
    }
}
