<?php

namespace App\Http\Controllers;

use App\Models\HomeImg;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    protected $hImage;

    public function __construct(HomeImg $hi) {
        $this->middleware('auth');
        $this->hImage = $hi;
    }

    public function index()
    {
        $data = $this->hImage->all();

        return view('admin.dashboard.home.index', compact('data'));
    }

    public function create()
    {
        return view('admin.dashboard.home.form');
    }

    public function store(Request $request)
    {
        $input = $request->all();

        $imageName = time().'.'.$input['image']->extension();  
        $input['image']->move(public_path('storage/home'), $imageName);

        $input['img'] = $imageName;
        
        $this->hImage->create($input);

        return redirect()->route('adm.home.index');
    }

    public function show($id)
    {
        $home = $this->hImage->find($id);

        return view('admin.dashboard.home.form', compact('home'));
    }

    public function update($id, Request $request)
    {
        $input = $request->all();
        if (isset($input['image'])) {
            $imageName = time().'.'.$input['image']->extension();  
            $input['image']->move(public_path('storage/home'), $imageName);
            $input['img'] = $imageName;
        }
        $home = $this->hImage->find($id)->update($input);

        return redirect()->route('adm.home.index');
    }

    public function delete($id)
    {
        $this->hImage->find($id)->delete();
        
        return redirect()->route('adm.home.index');
    }
}
