<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('admin.dashboard.index');
    }

    public function contact()
    {
        $data = Contact::orderBy('id', 'desc')->get();

        return view('admin.dashboard.contact', compact('data'));
    }
}
