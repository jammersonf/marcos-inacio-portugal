<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\PostCategory;
use Illuminate\Http\Request;

class PostController extends Controller
{
    protected $post;

    public function __construct(Post $p) {
        $this->middleware('auth');
        $this->post = $p;
    }

    public function index(Request $request)
    {
        $query = $request->q ?: '';
        $data = $this->post;
        if ($query) {
            $data = $data->where('title', 'like', "%{$query}%");
        }
        $data = $data->get();

        return view('admin.dashboard.posts.index', compact('data', 'query'));
    }

    public function create()
    {
        $categories = PostCategory::pluck('name', 'id')->toArray();

        return view('admin.dashboard.posts.form', compact('categories'));
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $imageName = time().'.'.$input['image']->extension();  
        $input['image']->move(public_path('storage/posts'), $imageName);

        $input['image'] = $imageName;
        $input['slug'] = \Str::slug($input['title'], '-');
        $input['user_id'] = auth()->user()->id;
        $this->post->create($input);

        return redirect()->route('adm.post.index');
    }

    public function show($id)
    {
        $post = $this->post->find($id);
        $categories = PostCategory::pluck('name', 'id')->toArray();

        return view('admin.dashboard.posts.form', compact('post', 'categories'));
    }

    public function update($id, Request $request)
    {
        $input = $request->all();
        $this->post->find($id)->update($input);

        return redirect()->route('adm.post.index');
    }

    public function delete($id)
    {
        $this->post->find($id)->delete();
        
        return redirect()->route('adm.post.index');
    }
}
