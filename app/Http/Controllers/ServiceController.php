<?php

namespace App\Http\Controllers;

use App\Models\About;
use App\Models\Number;
use App\Models\Service;
use App\Models\ServiceDesc;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    protected $service;

    public function __construct(Service $s) {
        $this->middleware('auth');
        $this->service = $s;
    }

    public function index()
    {
        $data = $this->service->all();

        return view('admin.dashboard.service.index', compact('data'));
    }

    public function create()
    {
        return view('admin.dashboard.service.form');
    }

    public function createAbout()
    {
        $pt = About::find(1);
        $en = About::find(2);

        return view('admin.dashboard.service.about', compact('pt', 'en'));
    }

    public function updateAbout(Request $request)
    {
        $input = $request->except(['_token']);

        if (isset($input['img'])) {
            $imageName = time().'.'.$input['img']->extension();  
            $input['img']->move(public_path('storage/about'), $imageName);

            $input['img'] = $imageName;
        } else {
            unset($input['img']);
        }
        
        About::whereLang($input['lang'])->update($input);

        toastr()->success('Salvo');

        return redirect()->route('adm.service.createAbout');
    }

    public function createNumber()
    {
        $pt = Number::find(1);
        $en = Number::find(2);

        return view('admin.dashboard.service.number', compact('pt', 'en'));
    }

    public function updateNumber(Request $request)
    {
        $input = $request->except(['_token']);
        
        Number::whereLang($input['lang'])->update($input);

        toastr()->success('Salvo');

        return redirect()->route('adm.service.createNumber');
    }

    public function createDesc()
    {
        $pt = ServiceDesc::find(1);
        $en = ServiceDesc::find(2);

        return view('admin.dashboard.service.desc', compact('pt', 'en'));
    }

    public function updateDesc(Request $request)
    {
        $desc = ServiceDesc::find(1)->update(['desc' => $request->pt]);
        $desc = ServiceDesc::find(2)->update(['desc' => $request->en]);

        toastr()->success('Salvo');

        return redirect()->route('adm.service.createDesc');
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $this->service->create($input);

        return redirect()->route('adm.service.index');
    }

    public function show($id)
    {
        $service = $this->service->find($id);

        return view('admin.dashboard.service.form', compact('service', 'data'));
    }

    public function update($id, Request $request)
    {
        $input = $request->all();
        $this->service->find($id)->update($input);

        return redirect()->route('adm.service.index');
    }

    public function delete($id)
    {
        $this->service->find($id)->delete();
        
        return redirect()->route('adm.service.index');
    }
}
