<?php

namespace App\Http\Controllers;

use App\Models\PostCategory;
use Illuminate\Http\Request;

class PostCategoryController extends Controller
{
    protected $postCategory;

    public function __construct(PostCategory $pc) {
        $this->middleware('auth');
        $this->postCategory = $pc;
    }

    public function index()
    {
        $data = $this->postCategory->all();
        
        return view('admin.dashboard.post_categories.index', compact('data'));
    }

    public function create()
    {
        return view('admin.dashboard.post_categories.form');
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $this->postCategory->create($input);

        return redirect()->route('adm.post.category.index');
    }

    public function show($id)
    {
        $category = $this->postCategory->find($id);

        return view('admin.dashboard.post_categories.form', compact('category'));
    }

    public function update($id, Request $request)
    {
        $input = $request->all();
        $this->postCategory->find($id)->update($input);

        return redirect()->route('adm.post.category.index');
    }

    public function delete($id)
    {
        $this->postCategory->find($id)->delete();
        
        return redirect()->route('adm.post.category.index');
    }
}
