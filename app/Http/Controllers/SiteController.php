<?php

namespace App\Http\Controllers;

use App\Mail\AdminContact;
use App\Mail\Contact as MailContact;
use App\Models\About;
use App\Models\Contact;
use App\Models\HomeImg;
use App\Models\Number;
use App\Models\Post;
use App\Models\PostCategory;
use App\Models\Service;
use App\Models\ServiceDesc;
use App\Models\Team;
use App\Models\TeamDesc;
use App\Models\Testimonial;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;

class SiteController extends Controller
{
    use SEOToolsTrait;

    public function __construct() {
    }

    public function index(Request $request)
    {
        $imgs = HomeImg::locale()->get();
        $services = Service::locale()->get();
        $testimonials = Testimonial::locale()->get();
        $number = Number::locale()->first();
        $team = Team::locale()->get();
        $teamDesc = TeamDesc::locale()->first();
        $about = About::locale()->first();
        $post = Post::locale()->orderDesc()->take(10)->get();
        $desc = ServiceDesc::locale()->first();
        
        return view('index', compact('imgs', 'services', 'testimonials', 'team', 'post', 'desc', 'teamDesc', 'number', 'about'));
    }
    public function construcao()
    {
        return view('building'); 
    }
    public function noticias(Request $request)
    {
        $cat = $request->cat ?: '';
        $query = $request->q;

        $posts = Post::locale()->orderDesc();
        if ($cat) {
            $posts = $posts->where('category_id', $cat);
        }
        if ($query) {
            $posts = $posts->where('title', 'like', "%{$query}%");   
        }
        $posts = $posts->paginate();
        $reads = Post::locale()->orderRead()->take(6)->get();
        $cats = PostCategory::locale()->get();

        $this->seo()->setTitle('Marcos Inácio - Publicações');
        $this->seo()->setDescription('Artigos');

        $this->seo()->opengraph()->setDescription('Artigos');
        $this->seo()->opengraph()->setTitle('Marcos Inácio - Publicações');

        return view('publicacoes', compact('posts', 'reads', 'cats', 'cat', 'query'));
    }

    public function article($slug)
    {
        $post = Post::bySlug($slug);
        $post->read += 1;
        $post->save();

        $related = Post::locale()->byCat($post->category_id);
        
        $this->seo()->setTitle($post->title);
        $this->seo()->setDescription($post->subtitle);

        $this->seo()->opengraph()->setDescription($post->subtitle);
        $this->seo()->opengraph()->setTitle($post->title);
        $this->seo()->opengraph()->setUrl(url('/noticias/'.$post->slug));
        $this->seo()->opengraph()->addProperty('type', 'article');
        $this->seo()->opengraph()->addProperty('locale', 'pt-br');
        $this->seo()->opengraph()->addProperty('locale:alternate', ['pt-pt', 'en-us']);
        $this->seo()->opengraph()->addImage(url('/storage/posts/'.$post->image), ['height' => 451, 'width' => 800]);

        return view('artigo', compact('post', 'related'));
    }

    public function contact(Request $request)
    {
        $input = $request->all();
        $input['lang'] = app()->getLocale();

        $message = Contact::create($input); 

        Mail::to('viniciusxavierpt@gmail.com')->send(new AdminContact($message));
        Mail::to($input['email'])->send(new MailContact($message));

        toastr()->success('Enviado. Logo entraremos em contato!');
        
        return back();
    }
}
