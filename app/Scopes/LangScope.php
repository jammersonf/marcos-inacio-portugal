<?php

namespace App\Scopes;

use Illuminate\Support\Facades\App;

trait LangScope
{
    public function scopeLocale($query)
    {
        $locale = App::getLocale();

        $query->where('lang', $locale);
    }
}
