<?php

namespace App\Models;

use App\Scopes\LangScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Number extends Model
{
    use LangScope;
    
    protected $fillable = [
        'lang',
        't1',
        'n1',
        't2',
        'n2',
        't3',
        'n3',
        't4',
        'n4',
        't5',
        'n5',
        't6',
        'n6',
    ];
}
