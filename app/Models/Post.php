<?php

namespace App\Models;

use App\Scopes\LangScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Post extends Model
{
    use LangScope;

    protected $fillable = [
        'title',
        'subtitle',
        'active',
        'slug',
        'image',
        'read',
        'text',
        'category_id',
        'lang',
        'user_id'
    ];

    protected $dates = ['created_at'];

    public function category()
    {
        return $this->belongsTo(PostCategory::class, 'category_id');
    }

    public function scopeOrderDesc($query)
    {
        return $query->orderBy('id', 'DESC');
    }

    public function scopeOrderRead($query)
    {
        return $query->orderBy('read', 'DESC');
    }

    public function scopeBySlug($query, $slug)
    {
        return $query->where('slug', $slug)->first();
    }

    public function scopeByCat($query, $cat)
    {
        return $query->where('category_id', $cat)->take(3)->get();
    }

    public function getCategoryNameAttribute()
    {
        return $this->category->name;
    }
}
