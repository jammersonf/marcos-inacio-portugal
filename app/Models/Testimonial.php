<?php

namespace App\Models;

use App\Scopes\LangScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Testimonial extends Model
{
    use LangScope;
    
    protected $fillable = [
        'name', 'text', 'lang'
    ];
}
