<?php

namespace App\Models;

use App\Scopes\LangScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Service extends Model
{
    use LangScope;

    protected $fillable = [
        'name', 'short', 'text', 'lang'
    ];
}
