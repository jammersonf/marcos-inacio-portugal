<?php

namespace App\Models;

use App\Scopes\LangScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class HomeImg extends Model
{
    use LangScope;

    protected $fillable = [
        'img', 'title', 'subtitle', 'title_support', 'support', 'lang'
    ];
}
