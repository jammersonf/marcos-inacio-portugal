<?php

namespace App\Models;

use App\Scopes\LangScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class TeamDesc extends Model
{
    use LangScope;
    
    protected $table = 'team_desc';
    
    protected $fillable = [
        'text', 'lang'
    ];
}
