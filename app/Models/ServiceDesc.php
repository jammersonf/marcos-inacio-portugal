<?php

namespace App\Models;

use App\Scopes\LangScope;
use Illuminate\Database\Eloquent\Model;

class ServiceDesc extends Model
{
    use LangScope;
    
    protected $table = 'service_desc';

    protected $fillable = [
        'desc', 'lang'
    ];
}
