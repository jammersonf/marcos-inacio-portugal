<?php

namespace App\Models;

use App\Scopes\LangScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Team extends Model
{
    use LangScope;
    
    protected $table = 'team';
    
    protected $fillable = [
        'name', 'job', 'image', 'text', 'lang'
    ];
}
