<?php

namespace App\Models;

use App\Scopes\LangScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class PostCategory extends Model
{
    use LangScope;

    protected $table = 'post_categories';

    protected $fillable = [
        'name', 'active', 'lang'
    ];
}
