<?php

namespace App\Models;

use App\Scopes\LangScope;
use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    use LangScope;
    
    protected $fillable = [
        'title', 'subtitle', 'desc', 'img', 'lang'
    ];
}
