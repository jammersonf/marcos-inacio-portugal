$('input').focus(function(){
    $(this).parents('.form-group').addClass('show-label');
});

$('input').blur(function(){
var inputValue = $(this).val();
    if ( inputValue == "" ) {
        $(this).removeClass('filled');
        $(this).parents('.form-group').removeClass('show-label');  
    } else {
        $(this).addClass('filled');
    }
})  
