$(document).ready(function(){
    var indicators = $('.indicator-slide');
    $('.lenght-text-slide').each(function () {
        $(this).text("0" + indicators.length)
    });
    $('.progress-bar-slide').each(function () {
        $(this).css('width', (1 / indicators.length * 100) + '%')
    });
    var indicatorsDepos = $('.indicator-depos');
    $('.lenght-text-depos').each(function () {
        $(this).text("0" + indicatorsDepos.length)
    });
    $('.progress-bar-depos').each(function () {
        $(this).css('width', (1 / indicatorsDepos.length * 100) + '%')
    });
    var indicatorsDeposMobile = $('.indicator-depos-mob');
    $('.lenght-text-depos-mob').each(function () {
        $(this).text("0" + indicatorsDepos.length)
    });
    $('.progress-bar-depos-mob').each(function () {
        $(this).css('width', (1 / indicatorsDepos.length * 100) + '%')
    });

});

$('#slide').on('slide.bs.carousel', function (e) {
    /* console.log(e); */
    var indicators = $('.indicator-slide');
    var currentSlide = e.to + 1;
    var lengthSlide = indicators.length;
    var progressBar = (currentSlide / lengthSlide) * 100;

    $('.current-text-slide').each(function () {
        $(this).text("0" + currentSlide);
    });
    $('.lenght-text-slide').each(function () {
        $(this).text("0" + lengthSlide);
    });
    $('.progress-bar-slide').each(function () {  
        $(this).css('width', progressBar + '%');
    });
})

$('#depos').on('slide.bs.carousel', function (e) {
    /* console.log(e); */
    var indicators = $('.indicator-depos');
    var currentSlide = e.to + 1;
    var lengthSlide = indicators.length;
    var progressBar = (currentSlide / lengthSlide) * 100;

    $('.current-text-depos').each(function () {
        $(this).text("0" + currentSlide);
    });
    $('.lenght-text-depos').each(function () {
        $(this).text("0" + lengthSlide);
    });
    $('.progress-bar-depos').each(function () {  
        $(this).css('width', progressBar + '%');
    });
})

$('#depos').on('slide.bs.carousel', function (e) {
    /* console.log(e); */
    var indicators = $('.indicator-depos-mob');
    var currentSlide = e.to + 1;
    var lengthSlide = indicators.length;
    var progressBar = (currentSlide / lengthSlide) * 100;

    $('.current-text-depos-mob').each(function () {
        $(this).text("0" + currentSlide);
    });
    $('.lenght-text-depos-mob').each(function () {
        $(this).text("0" + lengthSlide);
    });
    $('.progress-bar-depos-mob').each(function () {  
        $(this).css('width', progressBar + '%');
    });
})
