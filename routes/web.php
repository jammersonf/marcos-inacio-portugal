<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::name('home')->get('/', 'SiteController@index');
//Route::name('home')->get('/site', 'SiteController@index');
/* Route::name('construcao')->get('/construcao', 'SiteController@construcao'); */
Route::name('noticias')->get('/site/noticias', 'SiteController@noticias');
Route::name('artigo')->get('/site/noticias/{slug}', 'SiteController@article');

Auth::routes(['register' => false]);

Route::post('contact', 'SiteController@contact')->name('site.contact');

Route::prefix('admin')->name('adm.')->group(function () {
    Route::get('/', 'AdminController@index')->name('panel');
    Route::get('contatos', 'AdminController@contact')->name('contact');

    Route::prefix('inicio')->name('home.')->group(function() {
        Route::get('', 'HomeController@index')->name('index');
        Route::get('create', 'HomeController@create')->name('create');
        Route::post('create', 'HomeController@store')->name('store');
        Route::get('{id}', 'HomeController@show')->name('show');
        Route::post('{id}', 'HomeController@update')->name('update');
        Route::get('{id}/delete', 'HomeController@delete')->name('delete'); 
    });

    Route::prefix('servicos')->name('service.')->group(function() {
        Route::get('', 'ServiceController@index')->name('index');
        Route::get('create', 'ServiceController@create')->name('create');
        Route::post('create', 'ServiceController@store')->name('store');
        Route::get('descricao', 'ServiceController@createDesc')->name('createDesc');
        Route::post('descricao', 'ServiceController@updateDesc')->name('updateDesc');
        Route::get('sobre', 'ServiceController@createAbout')->name('createAbout');
        Route::post('sobre', 'ServiceController@updateAbout')->name('updateAbout');
        Route::get('numeros', 'ServiceController@createNumber')->name('createNumber');
        Route::post('numeros', 'ServiceController@updateNumber')->name('updateNumber');
        Route::get('{id}', 'ServiceController@show')->name('show');
        Route::post('{id}', 'ServiceController@update')->name('update');
        Route::get('{id}/delete', 'ServiceController@delete')->name('delete'); 
    });

    Route::prefix('depoimento')->name('testimonial.')->group(function() {
        Route::get('', 'TestimonyController@index')->name('index');
        Route::get('create', 'TestimonyController@create')->name('create');
        Route::post('create', 'TestimonyController@store')->name('store');
        Route::get('{id}', 'TestimonyController@show')->name('show');
        Route::post('{id}', 'TestimonyController@update')->name('update');
        Route::get('{id}/delete', 'TestimonyController@delete')->name('delete'); 
    });

    Route::prefix('profissionais')->name('team.')->group(function() {
        Route::get('', 'TeamController@index')->name('index');
        Route::get('create', 'TeamController@create')->name('create');
        Route::post('create', 'TeamController@store')->name('store');
        Route::get('descricao', 'TeamController@createDesc')->name('createDesc');
        Route::post('descricao', 'TeamController@updateDesc')->name('updateDesc');
        Route::get('{id}', 'TeamController@show')->name('show');
        Route::post('{id}', 'TeamController@update')->name('update');
        Route::get('{id}/delete', 'TeamController@delete')->name('delete'); 
    });

    Route::prefix('posts/categorias')->name('post.category.')->group(function() {
        Route::get('', 'PostCategoryController@index')->name('index');
        Route::get('create', 'PostCategoryController@create')->name('create');
        Route::post('create', 'PostCategoryController@store')->name('store');
        Route::get('{id}', 'PostCategoryController@show')->name('show');
        Route::post('{id}', 'PostCategoryController@update')->name('update');
        Route::get('{id}/delete', 'PostCategoryController@delete')->name('delete');
    });

    Route::prefix('posts')->name('post.')->group(function() {
        Route::get('', 'PostController@index')->name('index');
        Route::get('create', 'PostController@create')->name('create');
        Route::post('create', 'PostController@store')->name('store');
        Route::get('{id}', 'PostController@show')->name('show');
        Route::post('{id}', 'PostController@update')->name('update');
        Route::get('{id}/delete', 'PostController@delete')->name('delete');
    });
});
