@extends('components.layouts.articles')

@section('css')
    <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5ded3346754da500126f32b3&product=inline-share-buttons' async='async'></script>
@endsection

@section('content')
    @include('components.publicacoes.show')
@endsection
