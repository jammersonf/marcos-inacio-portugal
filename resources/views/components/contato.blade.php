<section class="p-0 contato section mobile-fit" id="contato">
  <div class="container-fluid">
    <div class="content-full">
    <div class="text-center  mobile-footer-offset">
      <div class="center">

        <div class="small-dots behind">
        </div>
      </div>
      
      <div class="row">
        <div class="d-sm-none col-md-4 col-lg-4 anim-down text-start mb-5 pb-3">
          <h1 class="text-white display-3 anim-down mb-5 pb-3">
            @lang('messages.contact.title2')
          </h1>
          <p class="text-white">Portugal | <strong>EUROPA</strong></p>
          <h2 class="text-orange">Lisboa</h2>
          <hr style=" border-top: 1.06104px solid #E6E6E6;">
          <div class="d-flex justify-content-between">
            <h5 class=" mt-2"><strong class="text-white">+351 933 342 355</strong></h5>
            <div>
              <a href="tel:+351933342355" target="_blank" class="btn text-white"><ion-icon size="large" name="call"></ion-icon></a>
              <a href="https://api.whatsapp.com/send?phone=351933342355" target="_blank" class="btn text-white"><ion-icon size="large" name="logo-whatsapp"></ion-icon></a>
              {{-- <button class="btn text-white"><i class="fab fa-whatsapp"></i></button> --}}
            </div>
          </div>
          <hr style=" border-top: 1.06104px solid #E6E6E6;">
          <div class="d-flex justify-content-between">
            <div>
              <p style="width: 200px;" class="text-white m-0">Avenida liberdade, 110, piso 4, sala 409
                1269-046 - Lisboa</p>
            </div>
              <div>
              <a href="https://goo.gl/maps/5UxGDFnc212bYLgx6" target="_blank" class="btn text-white"><ion-icon size="large" name="navigate"></ion-icon></a>
            </div>
          </div>
          <hr style=" border-top: 1.06104px solid #E6E6E6;">

        </div>

        <div class="col-md-6 col-lg-6 pl-md-0 pr-md-5  anim-down text-start">
          <h1 class="text-white display-3">
            @lang('messages.contact.title')
          </h1>
    
          <div style="max-width: 400px;" class="form-wrapper mr-md-5">
            {!! Form::open(['route' => 'site.contact', 'class' => 'text-start ']) !!}
              <div class="form-group m-0">
                <label for="input-name" >@lang('messages.contact.input1')</label>
                {{-- <input type="text" class="form-control " id="input-name" name="name" placeholder=""> --}}
                {!! Form::text('name', null, ['class' => 'form-control','id'=> 'input-name','name' => 'name', 'required']) !!}
              </div>
              <div class="form-group m-0">
                  <label for="input-email" >@lang('messages.contact.input2')</label>
                {{-- <input type="email" class="form-control" id="input-email"  name="email" placeholder=""> --}}
                {!! Form::text('email', null, ['class' => 'form-control','id'=> 'input-email','name' => 'email', 'required']) !!}
              </div>
              <div class="form-group  m-0">
                  <label for="input-phone" >@lang('messages.contact.input3')</label>
                {{-- <input type="text" class="phone form-control " id="input-phone" name="phone" placeholder=""> --}}
                {!! Form::text('phone', null, ['class' => 'form-control phone','id'=> 'input-phone','name' => 'phone', 'required']) !!}
              </div>
              <div class="d-none d-md-block text-start">
                <button class="btn btn-primary btn-lg btn-lg-submit py-3 my-3" type="submit"><strong>@lang('messages.contact.button')</strong></button>
              </div>
              <div class="d-md-none text-start">
                <button class="btn btn-primary btn-lg btn-block py-4  my-3" type="submit"><strong>@lang('messages.contact.button')</strong></button>
              </div>
            </form>
          </div>
        </div>
        <div class="d-none d-sm-block col-md-6 col-lg-6 pl-md-5 anim-down text-start">
          <div class="ml-md-5 mr-md-2">

          <h1 class="text-white display-3 anim-down mb-5 ">
            @lang('messages.contact.title2')
          </h1>
          <p class="text-white">Portugal | <strong>EUROPA</strong></p>
          <h3 class="text-orange">Lisboa</h3>
          <hr style=" border-top: 1.06104px solid #E6E6E6;margin-top: .5rem;
          margin-bottom: .51rem;">
          <div class="d-flex justify-content-between">
            <p class=" mt-2 mb-1 big-paragraph"><strong class="text-white">+351 933 342 355</strong></p>
            <div>
              <a href="tel:+351933342355" target="_blank" class="btn text-white p-1"><ion-icon style="font-size: 23px" name="call"></ion-icon></a>
              <a href="https://api.whatsapp.com/send?phone=351933342355" target="_blank" class="btn text-white p-1"><ion-icon style="font-size: 23px" name="logo-whatsapp"></ion-icon></a>
              {{-- <button class="btn text-white"><i class="fab fa-whatsapp"></i></button> --}}
            </div>
          </div>
          <hr style=" border-top: 1.06104px solid #E6E6E6; margin-top: .5rem;
          margin-bottom: .51rem;">
          <div class="d-flex justify-content-between">
            <div>
              <p style="width: 200px;" class="text-white m-0">Avenida liberdade, 110, piso 4, sala 409
                1269-046 - Lisboa</p>
            </div>
              <div>
              <a href="https://goo.gl/maps/5UxGDFnc212bYLgx6" target="_blank" class="btn text-white"><ion-icon style="font-size: 23px" name="navigate"></ion-icon></a>
            </div>
          </div>
          <hr style=" border-top: 1.06104px solid #E6E6E6; margin-top: .5rem;
          margin-bottom: .51rem;">
        </div>

        </div>
      </div>
      <h1 class="text-white display-3 social-text anim-up pt-md-0">Social</h1>
      <div class="d-flex justify-content-center icon-footer">
        <div class="text-center mx-2 anim-left">
          <button style="border-radius: 50px" class="btn btn-secondary btn-contatos"><i class="icon icon-facebook icon-2x face-pad-off"></i></button>
          <p class="text-gray py-1">facebook</p>
        </div>
        <div class="text-center mx-2 anim-right">
          <button style="border-radius: 50px" class="btn btn-secondary btn-contatos"><i class="icon icon-instagram icon-2x insta-pad-off"></i> </button>
          <p class="text-gray py-1">Instagram</p>
        </div>
      </div>

    </div>
    <div class="d-md-none anim-up">
        <div class="my-4"></div>

      <div class=" bottom-page text-center p-3 mx-5">
        <p><small class="float-left text-center" style="color: #939393;" >Marcos Inácio Advocacia © {{date('Y')}}  |  Política de Privacidade</small></p><br>  
        <a href="https://www.qualitare.com/home/"><img class="py-4" width="170px" src="{{asset('assets_fronts/imgs/marca_dagua.png')}}" alt=""></a> {{-- marca-dagua.png --}}
      </div>
    </div>
    <div class="d-none d-md-block anim-up">
      <div class=" bottom-page p-3 mx-5 d-flex justify-content-between pt-5">
        <p class="m-0"><small class="mt-3 float-left" style="color: #939393; bottom:0" >Marcos Inácio Advocacia © {{date('Y')}}  |  Política de Privacidade</small></p>
        <a href="https://www.qualitare.com/home/"><img style="right: 0" class="float-right" width="170px" src="{{asset('assets_fronts/imgs/marca_dagua.png')}}" alt=""></a> {{-- marca-dagua.png --}}
      </div>
    </div>
  </div>
  </div>
</section>
