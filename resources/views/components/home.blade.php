{{-- <section class="p-0  section" id="home">
    <div id="slide" class="carousel slide" data-ride="carousel">
        <div class="d-none d-md-block">

            <ol class="progress-carousel anim-down">
                <h2 class="text-orange current-text-slide">01</h2>
                <li style="width: 200px;" class="mx-15">
                    <div class="progress mt-3" style="height: 2px;">
                        <div class="progress-bar progress-bar-slide" role="progressbar" aria-valuenow="0"
                            aria-valuemin="0" aria-valuemax="100">
                        </div>
                    </div>
                </li>
                <h2 class="lenght-text-slide">-</h2>
                @foreach($imgs as $key => $img)
                <li data-target="#slide" data-slide-to="{{$key}}" class="indicator-slide d-none" @if($key==0)
                    class="active" @endif>
                </li>
                @endforeach
            </ol>

        </div>

        <div class="carousel-inner" role="listbox">
            @foreach($imgs as $key => $img)
            <div class="carousel-item carousel-item-home bg-{{$img->id}} @if($key == 0) active @endif"
                style="background-image: url('{{url('/storage/home/'.$img->img)}}')">
                <div class="slider-fade">
                    <img width="100%" height="100%" src="{{url('/assets_fronts/imgs/slider_fade.png')}}" alt="">
                </div>
                <div class="home-content tr home-texture ">
                </div>

                <div class="home-content tl anim-up animated fadeInUp">
                    <h1  class="text-white slide-title d-none d-md-block pt-md-5 mb-3" style="max-width: 600px;">{!! $img->title !!}</h1>
                    <h1  class="text-white slide-title d-md-none py-5 ">{!!$img->title!!}</h1>
                    <p  class="text-white d-none d-md-block" style="max-width: 400px">{{$img->subtitle}}</p>
                    <p  class="text-white d-md-none px-4">{{$img->subtitle}}</p>

                    <a style="cursor: pointer;" class="btn btn-primary btn-lg mt-5 p-4 moveToContato">
                        <strong>@lang('messages.about.contact')</strong>
                    </a>
                </div>
                <div class="d-none d-md-block home-content br anim-down animated fadeInDown">
                    <div class="d-flex justify-content-start ">

                        <div >
                            <img style="position: relative; top: 25px; right: 20px; z-index: 999"
                                src="{{asset('assets_fronts/imgs/title_bg.png')}}" alt="">
                            <p style="position:relative; z-index: 1000;" class="small-title text-white">
                                {{$img->title_support}}
                            </p>
                            <p class="text-white d-none d-md-block" style="max-width: 200px;">
                                <small>{{$img->support}}</small></p>
                        </div>
                    </div>

                </div>

            </div>
            @endforeach
        </div>
        <div class="d-none d-md-block ">

            <div class=" custom-carousel-control anim-down animated fadeInDown">
                <a class="btn-light mx-2 p-1" href="#slide" data-slide="prev">
                    <i class="icon icon-back-active text-white icon-round"></i>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="btn-light mx-2 p-1" href="#slide" data-slide="next">
                    <i class="icon icon-next-active text-white icon-round"></i>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>

        <ol class="progress-carousel progress-carousel-sm d-md-none anim-down animated fadeInDown">
            <h2 class="text-orange current-text-slide">01</h2>
            <li style="width: 60%; margin: 0px 12px" >
                <div class="progress mt-3" style="height: 2px;">
                    <div class="progress-bar progress-bar-slide" role="progressbar" aria-valuenow="0" aria-valuemin="0"
                        aria-valuemax="100">
                    </div>
                </div>
            </li>
            <h2 class="lenght-text-slide">-</h2>

        </ol>

    </div>

</section> --}}
<section class="p-0 section" id="home">
    <div id="slide" class="carousel slide" data-ride="carousel">
        <div class="d-none d-md-block">

            <ol class="progress-carousel anim-down">
                <h2 class="text-orange current-text-slide">01</h2>
                <li style="width: 200px;" class="mx-15">
                    <div class="progress mt-3" style="height: 2px;">
                        <div class="progress-bar progress-bar-slide" role="progressbar" aria-valuenow="0"
                            aria-valuemin="0" aria-valuemax="100">
                        </div>
                    </div>
                </li>
                <h2 class="lenght-text-slide">-</h2>
                @foreach($imgs as $key => $img)
                <li data-target="#slide" data-slide-to="{{$key}}" class="indicator-slide d-none" @if($key==0)
                    class="active" @endif>
                </li>
                @endforeach
            </ol>

        </div>

        <div class="carousel-inner" role="listbox">
            @foreach($imgs as $key => $img)
            <div class="carousel-item carousel-item-home bg-{{$img->id}} @if($key == 0) active @endif"
                style="background-image: url('{{url('/storage/home/'.$img->img)}}')">
                <div class="slider-fade">
                    <img width="100%" height="100%" src="{{url('/assets_fronts/imgs/slider_fade.png')}}" alt="">
                </div>
                <div class="home-content tr home-texture ">
                </div>

                <div class="home-content tl anim-up animated fadeInUp">
                    <h1  class="text-white slide-title d-none d-md-block pt-md-5 mb-3" style="max-width: 600px;">{!! $img->title !!}</h1>
                    <h1  class="text-white slide-title d-md-none py-5 ">{!!$img->title!!}</h1>
                    <p  class="text-white d-none d-md-block" style="max-width: 400px">{{$img->subtitle}}</p>
                    <p  class="text-white d-md-none px-4">{{$img->subtitle}}</p>

                    <a style="cursor: pointer;" class="btn btn-primary btn-lg mt-5 p-4 moveToContato">
                        <strong>@lang('messages.about.contact')</strong>
                    </a>
                </div>
                <div class="d-none d-md-block home-content br anim-down animated fadeInDown">
                    <div class="d-flex justify-content-start ">

                        <div >
                            <img style="position: relative; top: 25px; right: 20px; z-index: 999"
                                src="{{asset('assets_fronts/imgs/title_bg.png')}}" alt="">
                            <p style="position:relative; z-index: 1000;" class="small-title text-white">
                                {{$img->title_support}}
                            </p>
                            <p class="text-white d-none d-md-block" style="max-width: 200px;">
                                <small>{{$img->support}}</small></p>
                        </div>
                    </div>

                </div>

            </div>
            @endforeach
        </div>
        <div class="d-none d-md-block ">

            <div class=" custom-carousel-control anim-down animated fadeInDown">
                <a class="btn-light mx-2 p-1" href="#slide" data-slide="prev">
                    <i class="icon icon-back-active text-white icon-round"></i>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="btn-light mx-2 p-1" href="#slide" data-slide="next">
                    <i class="icon icon-next-active text-white icon-round"></i>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>

        <ol class="progress-carousel progress-carousel-sm d-md-none anim-down animated fadeInDown">
            <h2 class="text-orange current-text-slide">01</h2>
            <li style="width: 60%; margin: 0px 12px" >
                <div class="progress mt-3" style="height: 2px;">
                    <div class="progress-bar progress-bar-slide" role="progressbar" aria-valuenow="0" aria-valuemin="0"
                        aria-valuemax="100">
                    </div>
                </div>
            </li>
            <h2 class="lenght-text-slide">-</h2>

        </ol>

    </div>

</section>
