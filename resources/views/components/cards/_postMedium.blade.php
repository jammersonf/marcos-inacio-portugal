<div class="card-news-inline">
  <a  href="{{ route('artigo', $post->slug) }}" class="mb-auto">
    <div class="thumb" style="border-radius: 6px; filter: none; background-image: url('{{url('/storage/posts/'.$post->image)}}')"></div>
  </a>
  <div class="info">
    <span class="tag">{{$post->category->name}}</span>
    <a href="{{ route('artigo', $post->slug) }}">
      <h4 class="title title-light small font-weight-bold">{{$post->title}}</h4>
    </a>
    <span class="date">{{date('d/m/Y', strtotime($post->created_at))}}</span>
    <p>
      {{$post->subtitle}}
    </p>
  </div>
</div>
      
