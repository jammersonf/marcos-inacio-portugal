<div class="card-news">
  <a href="{{ route('artigo', $post->slug) }}">
    <span class="tag">{{$post->category->name}}</span>
    <div class="thumb" style="filter: none; background-image: url('{{url('/storage/posts/'.$post->image)}}'); border-radius: 6px;"></div>
    <div class="info">
      <span class="date">{{date('d/m/Y', strtotime($post->created_at))}}</span>
      <h4 class="title title-light small">{{$post->title}}</h4>
      <p>{{$post->subtitle}}</p>
    </div>
  </a>
</div>
    
