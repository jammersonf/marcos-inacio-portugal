<section class=" section mobile-fit overflow-hidden" id="depoimentos">
    <div class="container-fluid">
        <div style="height: 100%" class="content-full ">
            <div style="height: 100%" class="row">
                <div class="col-md-7 anim-left">

                        @lang('messages.depo')

                    {{-- slinder start --}}

                    <div style="position: unset;" id="depos" class="carousel  carousel-fade slide-depos"
                        data-ride="carousel">
                        <div class="d-md-none d-lg-block text-center mx-auto">

                        <ol class="progress-carousel-depos ">
                            <h2 class="text-orange current-text-depos">01</h2>
                            <li style="width: 200px;" class="mx-2">
                                <div class="progress mt-3" style="height: 2px;">
                                    <div class="progress-bar progress-bar-depos" role="progressbar" aria-valuenow="0"
                                        aria-valuemin="0" aria-valuemax="100">
                                    </div>
                                </div>
                            </li>
                            <h2 class="lenght-text-depos">-</h2>

                            @foreach($testimonials as $key => $data)
                                <li data-target="#depos" class="indicator-depos d-none" data-slide-to="{{$key}}" type=""
                                    @if($key==0) class="active" @endif>
                                </li>
                            @endforeach
                        </ol>
                    </div>


                        <div class="carousel-inner pb-5 " role="listbox">
                            @foreach($testimonials as $key => $data)
                                <div class=" carousel-item quote-center  slide carousel-item-depoimentos 
                                    @if($key == 0) active @endif">
                                        <i class="text-orange icon icon-quote icon-3x pb-2"></i>
                                        <blockquote class="text-white m-0">
                                            {{$data->text}}<br>
                                                <i style="transform: rotate(-180deg);"
                                                class=" text-orange icon quote-end-center icon-quote icon-3x pt-2 float-right"></i>
                                            <cite class=" pt-5">{{$data->name}}</cite>
                                        </blockquote>

                                </div>
                                {{-- <div class="d-md-none carousel-item  text-center carousel-item-depoimentos 
                                    @if($key == 0) active @endif">
                                        <i class="text-orange icon icon-quote icon-3x pb-2"></i> 
                                        <blockquote class="text-white m-0">
                                            {{$data->text}}<br>
                                                <i style="transform: rotate(-180deg); margin-right: 35%; margin-top: 30px;" class=" text-orange icon icon-quote icon-3x pt-2 float-right"></i>
                                            <cite class=" pt-5">{{$data->name}}</cite>
                                        </blockquote>

                                </div> --}}
                            @endforeach
                        </div>
                        <div class="d-none d-lg-block">
            
                            <div
                                class=" custom-carousel-control-left depoimentos-control d-flex justify-content-md-end">
                                <a style="box-shadow: none" class="btn btn-outline-primary mx-2 p-0" href="#depos"
                                    data-slide="prev">
                                    <i class="icon icon-back-active icon-round"></i>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a style="box-shadow: none" class="btn btn-outline-primary mx-2 p-0 " href="#depos"
                                    data-slide="next">
                                    <i class="icon icon-next-active  icon-round"></i>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>


                    </div>


                    {{-- slinder end --}}


                    {{-- <h1 class="text-orange">Depoimentos</h1>
                    <h1 class="text-white">dos nossos clientes</h1>

                    <p class="pt-4 text-white">
                        Anim amet id nulla in incididunt qui non culpa pariatur. Voluptate <br>
                        voluptate laborum incididunt pariatur ullamco dolore irure laboris <br>
                        sunt consequat magna. Velit esse ut minim proident mollit et aute <br>
                        occaecat. Esse consequat do do sint irure deserunt eiusmod.
                    </p> --}}
                </div>
                <div class=" col-md-5 " id="mousePara">
                    <div data-depth="0.6">

                        <img  class="d-none d-md-block  depoimentos-pais anim-right" src="{{asset('assets_fronts/imgs/pais.png')}}"
                        alt="">
                    </div>
                </div>
            </div>
            <div class="d-none d-md-block d-lg-none">
            <div class=" d-flex justify-content-lg-between float-right ">
                    <ol class="progress-carousel-depos progress-bar-tablet ">
                            <h2 class="text-orange current-text-depos-mob">01</h2>
                            <li style="width: 200px;" class="mx-2">
                                <div class="progress mt-3" style="height: 2px;">
                                    <div class="progress-bar progress-bar-depos-mob" role="progressbar" aria-valuenow="0"
                                        aria-valuemin="0" aria-valuemax="100">
                                    </div>
                                </div>
                            </li>
                            <h2 class="lenght-text-depos-mob">-</h2>

                            @foreach($testimonials as $key => $data)
                                <li data-target="#depos" class="indicator-depos-mob d-none" data-slide-to="{{$key}}" type=""
                                    @if($key==0) class="active" @endif>
                                </li>
                            @endforeach
                        </ol>


                    <div
                    class=" custom-carousel-control depoimentos-control d-flex justify-content-md-end">
                    <a style="box-shadow: none" class="btn btn-outline-primary mx-2 p-0" href="#depos"
                        data-slide="prev">
                        <i class="icon icon-back-active icon-round"></i>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a style="box-shadow: none" class="btn btn-outline-primary mx-2 p-0 " href="#depos"
                        data-slide="next">
                        <i class="icon icon-next-active  icon-round"></i>
                        <span class="sr-only">Next</span>
                    </a>
                </div>

            </div>
            </div>

        </div>

    </div>

</section>
