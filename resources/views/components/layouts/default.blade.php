<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Marcos Inácio</title>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="{{asset('assets_fronts/styles/main.css')}}?v=1.2">
    <link rel="stylesheet" href="{{asset('assets_fronts/styles/animate.css')}}">
    <link rel="stylesheet" href="{{asset('assets_fronts/styles/index.css')}}?v=1.2">
    {{-- <link rel="stylesheet" href="{{asset('assets_fronts/styles/fullpage.min.css')}}"> --}}
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets_fronts/icon-fonts/css/style.css')}}">
    {{-- favicon --}}
    <link rel="shortcut icon" href="{{ asset('assets_fronts/imgs/favicon.png') }}">
    <link href="https://unpkg.com/ionicons@4.5.10-0/dist/css/ionicons.min.css" rel="stylesheet">

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js"
        integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous">
    </script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js"
        integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous">
    </script>

    @toastr_css
    {!! SEO::generate() !!}
</head>

<body>
    {{-- navbar fixed top --}}
    <div class="container-fluid">
        <nav class=" d-lg-none navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="{{ route('home') }}"><i class="icon icon-logo icon-3x "></i></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
                aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <i class="icon icon-menu icon-2x pb-2"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav text-center">
                    <ul id="myMenu2" class="list-unstyled components">
                        <li data-menuanchor="home-btn">
                            <a class="nav-item nav-link active mobile-nav-item" href="#home-btn">
                                <h2>@lang('messages.menu1')</h2>
                            </a>
                        </li>
                        <li data-menuanchor="servicos-btn">
                            <a class="nav-item nav-link mobile-nav-item" href="#servicos-btn">
                                <h2>@lang('messages.menu2')</h2>
                            </a>
                        </li>
                        <li data-menuanchor="sobre-btn">
                            <a class="nav-item nav-link mobile-nav-item" href="#sobre-btn">
                                <h2>@lang('messages.menu3')</h2>
                            </a>
                        </li>
                        <li data-menuanchor="noticias-btn">
                            <a class="nav-item nav-link mobile-nav-item" href="#noticias-btn">
                                <h2>@lang('messages.menu4')</h2>
                            </a>
                        </li>
                        <li data-menuanchor="contato-btn">
                            <a class="nav-item nav-link mobile-nav-item" href="#contato-btn">
                                <h2>@lang('messages.menu5')</h2>
                            </a>
                        </li>
                        <li class="d-flex justify-content-center">
                            @lang('messages.btn-br')
                            @lang('messages.btn-en')
                        </li>
                        <li class=" d-none d-md-block pt-3">
                            <a style="background-color: #FE5000 !important; border-radius:6px"
                                href="http://www.marcosinacio.adv.br/" target="_blank"
                                class="btn px-4 py-2 btn-primary py-2 mt-2 mb-0 mx-0 px-0"><small>@lang('messages.footer')</small>
                            </a>
                        </li>

                    </ul>
                </div>
            </div>
        </nav>
    </div>
    {{-- sidebar --}}
    <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">
            <a href="{{ route('home') }}">

                <div class="sidebar-header text-center">
                    <?php include_once("assets_fronts/icons/logo.svg"); ?>
                </div>
            </a>
            <ul id="myMenu" class="list-unstyled components">
                <li data-menuanchor="home-btn" class="text-center home-btn">
                    <a href="#home-btn" style="cursor:pointer;"><i class="icon icon-home icon-2x "></i> <br>
                        @lang('messages.menu1')</a>
                </li>
                <li data-menuanchor="servicos-btn" class="text-center servicos-btn">
                    <a href="#servicos-btn" style="cursor:pointer;"><i class="icon icon-servicos icon-2x "></i> <br>
                        @lang('messages.menu2')</a>
                </li>
                <li data-menuanchor="sobre-btn" class="text-center sobre-btn">
                    <a href="#sobre-btn" style="cursor:pointer;"><i class="icon icon-sobre icon-2x "></i> <br>
                        @lang('messages.menu3')</a>
                </li>
                <li data-menuanchor="noticias-btn" class="text-center noticias-btn">
                    <a href="#noticias-btn" style="cursor:pointer;"><i class="icon icon-noticias icon-2x "></i> <br>
                        @lang('messages.menu4')</a>
                </li>
                <li data-menuanchor="contato-btn" class="text-center contato-btn">
                    <a href="#contato-btn" style="cursor:pointer;"><i class="icon icon-contato icon-2x "></i> <br>
                        @lang('messages.menu5')</a>
                </li>
            </ul>

            <ul class="list-unstyled CTAs p-0 nav-footer mb-0">
                <li class="d-flex justify-content-center">
                    @lang('messages.btn-side-br')
                    @lang('messages.btn-side-en')
                </li>
                <li>
                    <a href="http://www.marcosinacio.adv.br/" target="_blank"
                        class="btn btn-block btn-primary py-2 mt-2 mb-0 mx-0 px-0"><small>@lang('messages.footer')</small>
                    </a>
                </li>
            </ul>
        </nav>

        <!-- Page Content  -->
        <div id="content">
            <div style="padding-top: -10px;" class="d-lg-none "></div>
            {{-- <div id="fullpage"> --}}
            @yield('content')
            {{-- </div> --}}
        </div>
    </div>

    @jquery
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    {{-- <link rel="text/javascript" src="{{url('assets_fronts/js/jquery.visible.min.js')}}"> --}}
    <script src="{{asset('assets_fronts/js/jquery.mask.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.min.js"></script>
    @toastr_js
    @toastr_render

    <script src="{{asset('assets_fronts/js/fullpage.js')}}"> </script>
    <script src="{{asset('assets_fronts/js/custom.cards-scroll.js')}}"> </script>
    <script src="{{asset('assets_fronts/js/custom.carousel-indicators.js')}}"> </script>
    <script src="{{asset('assets_fronts/js/custom.profissionais-modal.js')}}"> </script>
    <script src="{{asset('assets_fronts/js/custom.label.js')}}"> </script>
    <script src="{{asset('assets_fronts/js/jquery.mousewheel.min.js')}}"> </script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js" type="module"></script>
    <script src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons.js"></script>

    {{-- <script src="{{asset('assets_fronts/js/countUp.min.js')}}"> </script> --}}
    {{-- <script src="{{asset('assets_fronts/js/index.js')}}"> </script> --}}

    {{-- form mask --}}
    <script>
        $(document).ready(function(){
            $('.phone').mask('(00) 00000-0000');
        });
    </script>
    <script>
        $(document).ready(function(){
            /* const countUp = new CountUp('sociosNumber', 0, 55);
            if (!countUp.error) {
                countUp.start();
            } else {
                console.error(countUp.error);
            } */

            /* import counterUp from 'counterup2'

            const el = document.querySelector( '.counter' )

            // Start counting, do this on DOM ready or with Waypoints.
            counterUp( el, {
                duration: 3000,
                delay: 16,
            } ) */

        });


    </script>
    @yield('scripts')

    <script>
        $(document).ready(function(){
            var animOnceUp = 0;
            var animOnceDown = 0;
            var animOnceLeft = 0;
            var animOnceRight = 0;

            var elementOthers =  document.querySelectorAll('#servicos .anim-up, #sobre .anim-up, #noticias .anim-up, #contato .anim-up') /*  #depoimentos .anim-up,  */
            elementOthers.forEach(e => {
                e.classList.add('hide-this')
            });
            elementOthers =  document.querySelectorAll('#servicos .anim-down, #sobre .anim-down, #noticias .anim-down, #contato .anim-down')/* #depoimentos .anim-down,  */
            elementOthers.forEach(e => {
                e.classList.add('hide-this')
            });

            elementOthers =  document.querySelectorAll('#servicos .anim-left, #sobre .anim-left,  #noticias .anim-left, #contato .anim-left')/* #depoimentos .anim-left, */
            elementOthers.forEach(e => {
                e.classList.add('hide-this')
            });
            elementOthers =  document.querySelectorAll('#servicos .anim-right, #sobre .anim-right, #depoimentos .anim-right, #noticias .anim-right, #contato .anim-right')
            elementOthers.forEach(e => {
                e.classList.add('hide-this')
            });

            $('#fullpage').fullpage({
                menu: ['#myMenu', '#myMenu2'],
                
                responsiveWidth: 700,
                anchors: ['home-btn', 'servicos-btn', 'sobre-btn', 'sobre-btn', 'noticias-btn', 'contato-btn'],/* 'servicos-btn', */ /* servicos-btn se refere a antiga sessão de depoimentos */
                parallax: false,
                scrollingSpeed: 1050,    
                normalScrollElements: '#sobre-wrapper',
                lazyLoading: true,
                onLeave: function(origin, destination, direction){
                    console.log(origin.item.id)
                    console.log(destination.item.id)
                    /* current items */
                    var element = [];

                    /* function handleAnimationEnd(node, animationName) {
                        node.classList.remove('animated', animationName)
                        node.classList.add('hide-this')
                        node.removeEventListener('animationend', handleAnimationEnd)

                        if (typeof callback === 'function') callback()
                    } */

                    /* ************** */
                    /* out animations */ 
                    /* ************** */

                    if($(window).width() >= 700){
                        if(origin.item.id != 'home' && destination.item.id != 'contato')
                            element =  document.querySelectorAll('#' + origin.item.id +' .anim-up')
                        element.forEach(e => {
                            e.classList.add('animated', 'fadeOutDown')
                            /* e.addEventListener('animationend',handleAnimationEnd(e, 'fadeOutDown')) */
                            e.addEventListener('animationend', function handleAnimationEnd() { 
                                e.classList.remove('animated', 'fadeOutDown')
                                e.classList.add('hide-this')
                                e.removeEventListener('animationend', handleAnimationEnd)
                            })

                        });
                        if(origin.item.id != 'home' && destination.item.id != 'contato')
                            element =  document.querySelectorAll('#' + origin.item.id +' .anim-down')
                        element.forEach(e => {
                            e.classList.add('animated', 'fadeOutUp')
                            /* e.addEventListener('animationend',handleAnimationEnd(e, 'fadeOutUp')) */
                            e.addEventListener('animationend', function handleAnimationEnd() { 
                                e.classList.remove('animated', 'fadeOutUp')
                                e.classList.add('hide-this')
                                e.removeEventListener('animationend', handleAnimationEnd)

                            })

                        });
                        if(origin.item.id != 'home' && destination.item.id != 'contato')
                            element =  document.querySelectorAll('#' + origin.item.id +' .anim-left')
                        element.forEach(e => {
                            e.classList.add('animated', 'fadeOutRight')
                            /* e.addEventListener('animationend',handleAnimationEnd(e, 'fadeOutRight')) */
                            e.addEventListener('animationend', function handleAnimationEnd() { 
                                e.classList.remove('animated', 'fadeOutRight')
                                e.classList.add('hide-this')
                                e.removeEventListener('animationend', handleAnimationEnd)

                            })

                        });
                        if(origin.item.id != 'home' && destination.item.id != 'contato')
                            element =  document.querySelectorAll('#' + origin.item.id +' .anim-right')
                        element.forEach(e => {
                            e.classList.add('animated', 'fadeOutLeft')
                            /* e.addEventListener('animationend',handleAnimationEnd(e, 'fadeOutLeft')) */
                            e.addEventListener('animationend', function handleAnimationEnd() { 
                                e.classList.remove('animated', 'fadeOutLeft')
                                e.classList.add('hide-this')
                                e.removeEventListener('animationend', handleAnimationEnd)

                            })

                        });
                    }

                    /* **************** */
                    /* Intro animations */ 
                    /* **************** */

                    element =  document.querySelectorAll('#' + destination.item.id +' .anim-up')
                    element.forEach(e => {

                            e.classList.remove('hide-this')
                            e.classList.remove('animated', 'fadeOutDown')

                            e.classList.add('animated', 'fadeInUp')
                            e.addEventListener('animationend', function handleAnimationEnd() { 
                                e.classList.remove('hide-this')

                                if($(window).width() >= 700){ 

                                    e.classList.remove('animated', 'fadeInUp')
                                }
                                e.removeEventListener('animationend', handleAnimationEnd)

                            })

                    });
                    element =  document.querySelectorAll('#' + destination.item.id +' .anim-down')
                    element.forEach(e => {

                            e.classList.remove('hide-this')
                            e.classList.remove('animated', 'fadeOutUp')

                            e.classList.add('animated', 'fadeInDown')
                            e.addEventListener('animationend', function handleAnimationEnd() { 
                                e.classList.remove('hide-this')

                                if($(window).width() >= 700){ 

                                e.classList.remove('animated', 'fadeInDown')
                                }
                                e.removeEventListener('animationend', handleAnimationEnd)

                            })
                    });
                    element =  document.querySelectorAll('#' + destination.item.id +' .anim-left')
                    element.forEach(e => {


                        e.classList.remove('hide-this')
                            e.classList.remove('animated', 'fadeOutRight')

                            e.classList.add('animated', 'fadeInLeft')
                            e.addEventListener('animationend', function handleAnimationEnd() { 
                                e.classList.remove('hide-this')

                                if($(window).width() >= 700){ 

                                    e.classList.remove('animated', 'fadeInLeft')
                                }
                                e.removeEventListener('animationend', handleAnimationEnd)

                            })
                    });
                    element =  document.querySelectorAll('#' + destination.item.id +' .anim-right')
                    element.forEach(e => {

                        e.classList.remove('hide-this')
                            e.classList.remove('animated', 'fadeOutLeft')

                            e.classList.add('animated', 'fadeInRight')
                            e.addEventListener('animationend', function handleAnimationEnd() { 
                                e.classList.remove('hide-this')

                                if($(window).width() >= 700){ 
                                    e.classList.remove('animated', 'fadeInRight')
                                 }
                                e.removeEventListener('animationend', handleAnimationEnd)

                            })
                    });

                    /* animação de numeros */
                    if(destination.item.id == "numeros"){
                        $({ Counter: 0 }).animate({
                            Counter: $('.Single').text()
                        }, {
                            duration: 1000,
                            easing: 'swing',
                            step: function() {
                                $('.Single').text(Math.ceil(this.Counter));
                            }
                        });

                        $({ Counter: 0 }).animate({
                            Counter: $('.clientesNumb').text()
                        }, {
                            duration: 1000,
                            easing: 'swing',
                            step: function() {
                                $('.clientesNumb').text(Math.ceil(this.Counter));
                            }
                        });

                        $({ Counter: 0 }).animate({
                            Counter: $('.advogadosNumber').text()
                        }, {
                            duration: 1000,
                            easing: 'swing',
                            step: function() {
                                $('.advogadosNumber').text(Math.ceil(this.Counter));
                            }
                        });

                        $({ Counter: 0 }).animate({
                            Counter: $('.paisesNumber').text()
                        }, {
                            duration: 1000,
                            easing: 'swing',
                            step: function() {
                                $('.paisesNumber').text(Math.ceil(this.Counter));
                            }
                        });

                        $({ Counter: 0 }).animate({
                            Counter: $('.envolvidosNumber').text()
                        }, {
                            duration: 1000,
                            easing: 'swing',
                            step: function() {
                                $('.envolvidosNumber').text(Math.ceil(this.Counter));
                            }
                        });

                        $({ Counter: 0 }).animate({
                            Counter: $('.experienciaNumber').text()
                        }, {
                            duration: 1000,
                            easing: 'swing',
                            step: function() {
                                $('.experienciaNumber').text(Math.ceil(this.Counter));
                            }
                        });

                    }
                },
            });
            $('.moveToContato').click(
                function () { 
                    $.fn.fullpage.moveTo('contato-btn');
                }
            );
            $('.modal-team').on('show.bs.modal', function (e) {
                console.log("nodal aberto");
                /* $('body').addClass('stop-scrolling') */
                $.fn.fullpage.setAllowScrolling(false, 'all'); 

                /* console.log(full); */
            })     
            $('.modal-team').on('hidden.bs.modal', function (e) {
                console.log("nodal fechado");
                $.fn.fullpage.setAllowScrolling(true, 'all'); 
                /* $('body').removeClass('stop-scrolling') */
                /* console.log(full); */

            })     
        });
    </script>

    <script>
        $("#sobre-wrapper").mousewheel(function(evt, chg) {
            this.scrollLeft -= (chg * 30); //need a value to speed up the change
            evt.preventDefault();
        });
    </script>

    <script>
        $('#myMenu2>li>a').on('click', function(){
            $('#navbarNavAltMarkup').collapse('hide');
        });
    </script>
</body>

</html>