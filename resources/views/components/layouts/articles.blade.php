<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Marcos Inácio</title>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="{{asset('assets_fronts/styles/main.css')}}?v=1.2">
    <link rel="stylesheet" href="{{asset('assets_fronts/styles/index.css')}}?v=1.2">
    <link rel="stylesheet" href="{{asset('assets_fronts/icon-fonts/css/style.css')}}?v=1.2">
    <link rel="stylesheet" href="{{asset('assets_fronts/styles/publicacoes.css')}}?v=1.2">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

    {{-- favicon --}}
    <link rel="shortcut icon" href="{{ asset('assets_fronts/imgs/favicon.png') }}">

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js"
        integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous">
    </script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js"
        integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous">
    </script>
    @toastr_css
    {!! SEO::generate() !!}
    {!! OpenGraph::generate() !!}
    @yield('css')
</head>

<body>
    {{-- navbar fixed top --}}
    <nav class=" d-lg-none navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="{{ route('home') }}"><i class="icon icon-logo icon-3x "></i></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <i class="icon icon-menu icon-2x pb-2"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav text-center">
                <ul id="myMenu2" class="list-unstyled components">
                    <li data-menuanchor="home-btn" >
                        <a class="nav-item nav-link  mobile-nav-item"  href="{{ route('home') }}">
                            <h2>@lang('messages.menu1')</h2>
                        </a>
                    </li>
                    <li data-menuanchor="servicos-btn">
                        <a class="nav-item nav-link mobile-nav-item"  
                            href="{{ route('home') }}">
                            <h2>@lang('messages.menu2')</h2>
                        </a>
                    </li>
                    <li ata-menuanchor="sobre-btn">
                        <a class="nav-item nav-link mobile-nav-item"  
                            href="{{ route('home') }}">
                            <h2>@lang('messages.menu3')</h2>
                        </a>
                    </li>
                    <li data-menuanchor="noticias-btn">
                        <a class="nav-item nav-link active mobile-nav-item"  
                            href="{{ route('noticias') }}">
                            <h2>@lang('messages.menu4')</h2>
                        </a>
                    </li>
                    <li data-menuanchor="contato-btn">
                        <a class="nav-item nav-link mobile-nav-item"  
                            href="{{ route('home') }}">
                            <h2>@lang('messages.menu5')</h2>
                        </a>
                    </li>
                    <li class="d-flex justify-content-center">
                            <a style="border-radius: 6px" href="?lang=pt" class="mx-1 px-4 py-3 btn btn-outline-secondary">PT</a>
                            <a style="border-radius: 6px" href="?lang=en" class="mx-1 px-4 py-3 btn btn-outline-secondary">EN</a>
                        </li>
                        <li class=" d-none d-md-block pt-3">
                            <a style="background-color: #FE5000 !important; border-radius:6px" href="http://www.marcosinacio.adv.br/" target="_blank"
                                class="btn px-4 py-2 btn-primary py-2 mt-2 mb-0 mx-0 px-0"><small>@lang('messages.footer')</small>
                            </a>
                        </li>
        
                </ul>
            </div>
        </div>
    </nav>
    {{-- sidebar --}}
    <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">
            <a href="{{ route('home') }}">
                    <div class="sidebar-header text-center">
                        <?php include_once("assets_fronts/icons/logo.svg"); ?>
                    </div>
                </a>

            <ul class="list-unstyled components">
                <li id="home-btn" class="text-center">
                    <a href="{{ route('home') }}"><i class="icon icon-home icon-2x "></i> <br> @lang('messages.menu1')</a>
                </li>
                <li id="servicos-btn" class="text-center">

                    <a href="{{ route('home') }}"><i class="icon icon-servicos icon-2x "></i> <br> @lang('messages.menu2')</a>
                </li>
                <li id="sobre-btn" class="text-center">

                    <a href="{{ route('home') }}"><i class="icon icon-sobre icon-2x "></i> <br> @lang('messages.menu3')</a>
                </li>
                <li id="noticias-btn" class="text-center active">

                    <a href="{{ route('noticias') }}"><i class="icon icon-noticias icon-2x "></i> <br> @lang('messages.menu4')</a>
                </li>
                <li id="contato-btn" class="text-center">

                    <a href="{{ route('home') }}"><i class="icon icon-contato icon-2x "></i> <br> @lang('messages.menu5')</a>
                </li>
            </ul>

            <ul class="list-unstyled CTAs p-0 nav-footer mb-0">
                <li class="d-flex justify-content-center">
                    <a href="?lang=pt" class="mx-1  btn btn-outline-secondary">PT</a>
                    <a href="?lang=en" class="mx-1  btn btn-outline-secondary">EN</a>
                </li>
                <li>
                    <a href="http://www.marcosinacio.adv.br/" target="_blank"
                        class="btn btn-block btn-primary py-2 mt-2 mb-0 mx-0 px-0"><small>ACESSE O PORTAL <br>
                            BRASILEIRO</small> </a>
                </li>
            </ul>
        </nav>

        <!-- Page Content  -->
        <div id="content">
            {{-- <div class="d-lg-none pt-5 mt-4"></div> --}}
                @yield('content')
        </div>
    </div>

    @jquery
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    {{-- <link rel="text/javascript" src="{{url('assets_fronts/js/jquery.visible.min.js')}}"> --}}
    <script src="{{asset('assets_fronts/js/jquery.mask.min.js')}}"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

    @toastr_js
    @toastr_render

    <script src="{{asset('assets_fronts/js/custom.cards-scroll.js')}}"> </script>
    <script src="{{asset('assets_fronts/js/custom.carousel-indicators.js')}}"> </script>
    <script src="{{asset('assets_fronts/js/custom.profissionais-modal.js')}}"> </script>
    <script src="{{asset('assets_fronts/js/custom.label.js')}}"> </script>

    {{-- form mask --}}
    <script>
        $(document).ready(function(){
            $('.phone').mask('(00) 00000-0000');
        });
    </script>
    <script>
        AOS.init();
    </script>
    
</body>

</html>
