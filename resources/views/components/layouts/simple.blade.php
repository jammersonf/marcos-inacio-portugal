<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Marcos Inácio</title>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="{{asset('assets_fronts/styles/main.css')}}?v=1.2">
    <link rel="stylesheet" href="{{asset('assets_fronts/styles/animate.css')}}">
    <link rel="stylesheet" href="{{asset('assets_fronts/styles/index.css')}}?v=1.2">
    {{-- <link rel="stylesheet" href="{{asset('assets_fronts/styles/fullpage.min.css')}}"> --}}
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets_fronts/icon-fonts/css/style.css')}}">
    {{-- favicon --}}
    <link rel="shortcut icon" href="{{ asset('assets_fronts/imgs/favicon.png') }}">

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js"
        integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous">
    </script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js"
        integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous">
    </script>
    @toastr_css
    {!! SEO::generate() !!}
</head>

<body>
    {{-- navbar fixed top --}}
    <div class="container-fluid p-0">
        @yield('content')
    </div>
    @jquery
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    {{-- <link rel="text/javascript" src="{{url('assets_fronts/js/jquery.visible.min.js')}}"> --}}
    <script src="{{asset('assets_fronts/js/jquery.mask.min.js')}}"></script>

    @toastr_js
    @toastr_render

    <script src="{{asset('assets_fronts/js/fullpage.js')}}"> </script>
    <script src="{{asset('assets_fronts/js/custom.cards-scroll.js')}}"> </script>
    <script src="{{asset('assets_fronts/js/custom.carousel-indicators.js')}}"> </script>
    <script src="{{asset('assets_fronts/js/custom.profissionais-modal.js')}}"> </script>
    <script src="{{asset('assets_fronts/js/custom.label.js')}}"> </script>
    <script src="{{asset('assets_fronts/js/jquery.mousewheel.min.js')}}"> </script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

    {{-- form mask --}}
    <script>
        $(document).ready(function(){
            $('.phone').mask('(00) 00000-0000');
        });
    </script>
    <script>
        $(document).ready(function(){
            /* var full = new fullpage('#fullpage', {
                menu: ['#myMenu', '#myMenu2'],
                responsiveWidth: 700,
                anchors: ['home-btn', 'servicos-btn','servicos-btn', 'sobre-btn', 'noticias-btn', 'contato-btn'],
                parallax: true,
                normalScrollElements: '#sobre-wrapper',
                lazyLoading: true,
                onLeave: function(origin, destination, direction){
                },
            });    */
            $('#fullpage').fullpage({
                menu: ['#myMenu', '#myMenu2'],
                responsiveWidth: 700,
                anchors: ['home-btn', 'servicos-btn','servicos-btn', 'sobre-btn', 'noticias-btn', 'contato-btn'],
                parallax: true,
                normalScrollElements: '#sobre-wrapper',
                lazyLoading: true,
                onLeave: function(origin, destination, direction){
                },
            });

            $('.modal-team').on('show.bs.modal', function (e) {
                console.log("nodal aberto");
                /* $('body').addClass('stop-scrolling') */
                $.fn.fullpage.setAllowScrolling(false, 'all'); 

                /* console.log(full); */
            })     
            $('.modal-team').on('hidden.bs.modal', function (e) {
                console.log("nodal fechado");
                $.fn.fullpage.setAllowScrolling(true, 'all'); 
                /* $('body').removeClass('stop-scrolling') */
                /* console.log(full); */

            })     
        });
    </script>

    <script>
        $("#sobre-wrapper").mousewheel(function(evt, chg) {
            this.scrollLeft -= (chg * 30); //need a value to speed up the change
            evt.preventDefault();
        });
    </script>

    <script>
        $('#myMenu2>li>a').on('click', function(){
            $('#navbarNavAltMarkup').collapse('hide');
        });
    </script>
    <script>
        AOS.init();
    </script>
</body>

</html>