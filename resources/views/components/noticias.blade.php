<section class="p-0 section mobile-fit" id="noticias">
    <div class="container-fluid p-0 ">
        <div class="d-none d-md-block content-title no-padding-md anim-down">
            @lang('messages.news.title')
        </div>
        <div class="d-md-none content-title no-padding-md text-center anim-down">
            @lang('messages.news.title')
        </div>
        <div style="min-height: 400px;" class="scrolling-wrapper anim-left" id="noticias-wrapper">
            @foreach($post as $data)
            <div class="card card-noticias mx-3"> {{-- position:relative; top: 0; --}}
                    <a href="{{ route('artigo', $data->slug) }}">

                <div style="
                    background-image: url('{{url('/storage/posts/'.$data->image)}}');
                    background-size: cover;
                    background-position: center;
                    width: 90%; 
                    height: 170px;
                    border-radius: 6px;
                    margin-left: 5%;
                    " class="noticia-card-offset text-center">
                    {{-- <div style="
                        background-image: url('{{url('/storage/posts/'.$data->image)}}');
                        background-size: cover;
                        background-position: center;
                        border-radius: 6px;
                        width=90%; 
                        position: relative;
                    "></div> --}}
                    {{-- <img style="border-radius: 6px" width="90%" src="{{url('/storage/posts/'.$data->image)}}" alt=""> --}}
                </div>
            </a>
                <div class="card-body">
                    <a href="{{ route('artigo', $data->slug) }}">
                            <h3 style="white-space: pre-wrap; " class="card-title p-1 mb-5 text-white">{{$data->title}}</h3>
                        </a>
                    <p style="white-space: pre-wrap; " class="card-text  text-white px-1">
                        <small class="float-left">{{ date('d/m/Y', strtotime($data->created_at))}}</small></p>
                </div>
            </div>
            @endforeach
        </div>
        <div class="d-none d-md-block">
            <div class="d-none d-lg-block br-ab anim-left">
                <div class=" d-flex justify-content-end">
                    <a class="btn btn-outline-primary mx-2 p-0" id="left-button-noticias">
                        <i class="icon icon-back-active  icon-round "></i>
                    </a>
                    <a class="btn btn-outline-primary mx-2 p-0" id="right-button-noticias">
                        <i class="icon icon-next-active  icon-round "></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="d-none d-md-block bl anim-right">
            <a href="{{ route('noticias') }}"
                class="btn btn-orange text-orange btn-lg">@lang('messages.news.footer')</a>
        </div>
        <div class="d-md-none text-center b "> {{-- anim-right --}}
            <a href="{{ route('noticias') }}"
                class="btn btn-orange text-orange btn-lg">@lang('messages.news.footer')</a>
        </div>

    </div>
</section>
