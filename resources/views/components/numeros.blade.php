<section class=" section mobile-fit overflow-hidden" id="numeros">
    <div class="container-fluid">
        <div class="content-title anim-up ">
            @lang('messages.about.numbers')
        </div>
    </div>
    <div class="container">

        <div class="row anim-up ml-sm-5 pl-sm-5 numbers-row">
            <div class="col-6 col-md-4">
                <div class=" number " id="sociosData">
                    <div data-depth="0.04">
                        <h1 class="display-1 text-white no-wrap">
                            <span class="Single">{{$number->n1}}</span>
                        </h1>
                        <h5 style="max-width: 230px;"class="text-white text-left">
                            {!! $number->t1 !!}
                        </h5>
                    </div>
                </div>
                <div class="dot-bg-holder">
                    <div style="right: 45px;" class="dot-bg" id="sociosBg">
                        <div data-depth="0.1">
                            <img class="" src="{{url('/assets_fronts/imgs/numbers_bg.png')}}" alt="">
                        </div>
                    </div>
                </div>
                <div style="height: 15vh;" class="d-none d-md-block"></div>
            </div>
            <div class="col-6 col-md-4">
                <div class="clientesNumber number " id="clientesData">
                    <div data-depth="0.04">
                        <h1 class=" display-1 text-white no-wrap">
                            <span class="clientesNumb">{{$number->n2}}</span>
                        </h1>
                        <h5 style="max-width: 230px;" class="text-white text-left">
                            {!! $number->t2 !!}
                        </h5>
                    </div>
                </div>
                <div class="dot-bg-holder">
                    <div style="right: 70px;" class="dot-bg" id="clientesBg">
                        <div data-depth="0.2">
                            <img class="" src="{{url('/assets_fronts/imgs/numbers_bg.png')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-4">
                <div class=" number " id="advogadosData">
                    <div data-depth="0.02">
                        <h1 class="  display-1 text-white no-wrap advogadosNumber">
                            {{$number->n3}}
                        </h1>
                        <h5 style="max-width: 230px;" class="text-white text-left">
                            {!! $number->t3 !!}
                        </h5>
                    </div>
                </div>
                <div class="dot-bg-holder">
                    <div style="right: 90px;" class="dot-bg" id="advogadosBg">
                        <div data-depth="0.05">
                            <img class="" src="{{url('/assets_fronts/imgs/numbers_bg.png')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-4">
                <div class=" number " id="paisesData">
                    <div data-depth="0.03">
                        <h1 class="  display-1 text-white  no-wrap paisesNumber">
                            {{$number->n4}}
                        </h1>
                        <h5 style="max-width: 230px;" class="text-white text-left ">
                            {!! $number->t4 !!}
                        </h5>
                    </div>
                </div>
                <div class="dot-bg-holder">
                    <div style="right: 90px;" class="dot-bg" id="paisesBg">
                        <div data-depth="0.1">
                            <img class="" src="{{url('/assets_fronts/imgs/numbers_bg.png')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-4">
                <div class=" number " id="envolvidosData">
                    <div data-depth="0.01">
                        <h1 class="  display-1 text-white no-wrap">
                            <span class="envolvidosNumber">{{$number->n5}}</span>
                        </h1>
                        <h5 style="max-width: 230px;" class="text-white text-left">
                            {!! $number->t5 !!}
                        </h5>
                    </div>
                </div>
                <div class="dot-bg-holder">
                    <div style="right: 10px;" class="dot-bg" id="envolvidosBg">
                        <div data-depth="0.2">
                            <img class="" src="{{url('/assets_fronts/imgs/numbers_bg.png')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-4">
                <div class=" number " id="experienciaData">
                    <div data-depth="0.04">
                        <h1 class=" display-1 text-white no-wrap">
                            <span class="experienciaNumber">{{$number->n6}}</span>
                        </h1>
                        <h5  style="max-width: 230px;" class="text-white text-left ">
                            {!! $number->t6 !!}
                        </h5>
                    </div>
                </div>
                <div class="dot-bg-holder">
                    <div style="right: 90px;" class="dot-bg" id="experienciaBg">
                        <div data-depth="0.15">
                            <img class="" src="{{url('/assets_fronts/imgs/numbers_bg.png')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@section('numeros-scripts')


<script>
    $(document).ready(function(){
        var scene = document.getElementById('sociosData');
        var parallaxInstance = new Parallax(scene, {
            relativeInput: true
        });
        scene = document.getElementById('sociosBg');
        parallaxInstance = new Parallax(scene, {
            relativeInput: true
        });
        scene = document.getElementById('clientesData');
        parallaxInstance = new Parallax(scene, {
            relativeInput: true
        });
        scene = document.getElementById('clientesBg');
        parallaxInstance = new Parallax(scene, {
            relativeInput: true
        });
        scene = document.getElementById('advogadosData');
        parallaxInstance = new Parallax(scene, {
            relativeInput: true
        });
        scene = document.getElementById('advogadosBg');
        parallaxInstance = new Parallax(scene, {
            relativeInput: true
        });
        scene = document.getElementById('paisesData');
        parallaxInstance = new Parallax(scene, {
            relativeInput: true
        });
        scene = document.getElementById('paisesBg');
        parallaxInstance = new Parallax(scene, {
            relativeInput: true
        });
        scene = document.getElementById('envolvidosData');
        parallaxInstance = new Parallax(scene, {
            relativeInput: true
        });
        scene = document.getElementById('envolvidosBg');
        parallaxInstance = new Parallax(scene, {
            relativeInput: true
        });
        scene = document.getElementById('experienciaData');
        parallaxInstance = new Parallax(scene, {
            relativeInput: true
        });
        scene = document.getElementById('experienciaBg');
        parallaxInstance = new Parallax(scene, {
            relativeInput: true
        });

    });
</script>

@endsection
