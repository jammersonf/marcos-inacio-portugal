<div class="container-fluid">
  <div class="content-full">
    <div class="row">
      <div class="col-md-6">

        @lang('messages.news.title2')
      </div>
      <div class="col-md-6">
        
        <form class="d-lg-none pt-5" action="#">
          <div class="box-search">
            <input type="text" name="q" value="{{$query}}" placeholder="Buscar" autofocus>
            <button><i class="fa fa-search" aria-hidden="true"></i></button>
          </div>
        </form>
      </div>
    </div>

    <div class="row pt-5 mt-5">
      <div class="col-lg-8">
        @foreach($posts as $key => $post)
          @if($key <= 0)
            @include('components.cards._post')
          @else
            @include('components.cards._postMedium')
          @endif
        @endforeach
  
      {!! $posts->links() !!}

      </div>
      <div class="d-none d-lg-block col-lg-4">
        @include('components.partials.aside')
      </div>
    </div>
  </div>
</div>
    
