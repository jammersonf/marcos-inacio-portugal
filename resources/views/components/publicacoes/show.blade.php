<div class="container px-md-5">
  <article>
    <div class="article-date text-center pt-5 mt-5">
      <p class="paragraph-small text-uppercase">{{$post->created_at->isoFormat('LL')}}</p>

    </div>
    <div class="article-title text-center py-5">
      <h1>{{$post->title}}</h1>

    </div>
    {{-- <form action="" class="view-tablet">
      <div class="box-search">
        <input type="text" placeholder="Buscar">
        <button><i class="fa fa-search" aria-hidden="true"></i></button>
      </div>
    </form> --}}
</article>
</div>
    <div class="d-none d-lg-block container-fluid px-md-5">
        <div class="card-news">
            <a href="">
              {{-- <span class="tag tag--bottom">lorem ipsum</span> --}}
              <div class="thumb thumb-full"
                style="filter: none; background-image: url('{{url('/storage/posts/'.$post->image)}}')"></div>
            </a>
          </div>  
    </div>
    <div class="d-lg-none card-news">
      <a href="">
        {{-- <span class="tag tag--bottom">lorem ipsum</span> --}}
        <div class="thumb thumb-full"
          style="filter: none; background-image: url('{{url('/storage/posts/'.$post->image)}}')"></div>
      </a>
    </div>
<div class="container px-md-5">
  <article>
    <div class="row">
      <div class="col-md-2 d-none d-lg-block ">
        <div style="transform: rotate(-90deg);" class="box-share">
          <p class="pl-5 ml-2">@lang('messages.news.share')</p>
          <div class="sharethis-inline-share-buttons"></div>

        </div>

      </div>
      <div class="col-lg-9">
        <div>

          {!! $post->text !!}
        </div>
        <hr>
        <div class="d-lg-none">
            <div class="py-4 ">
              <p><strong> @lang('messages.news.share')</strong></p>
              <div class="sharethis-inline-share-buttons"></div>
            </div>
          </div>
          <div class="well well-lg py-5 my-3" id="comment">

              <h4>@lang('messages.news.comment')</h4>
    
              <div id="fb-root"></div>
              <script>
                (function(d, s, id) {
                              var js, fjs = d.getElementsByTagName(s)[0];
                              if (d.getElementById(id)) return;
                              js = d.createElement(s); js.id = id;
                              js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.10";
                              fjs.parentNode.insertBefore(js, fjs);
                          }(document, 'script', 'facebook-jssdk'));
              </script>
              <div class="fb-comments" data-href="{{url('/noticias/'.$post->slug)}}" data-width="100%" data-numposts="5">
              </div>
            </div>
    
      </div>
    </div>


  </article>

  
</div>
<div style="overflow-x: hidden;" class=" p-0 m-0">

  @include('components.contato')
</div>

{{-- sessao de artigos relacionados dentro de um artigo retirado --}}
{{-- 
<div class="section" id="relacionados">
<div class="container">

  <div class="content-full">
    @lang('messages.news.title3')
    <div class="pt-5 mt-5">
        <div class="row">
          <div class="col-md-9">
            @foreach($related as $post)
              @include('components.cards._postMedium-temp')
            @endforeach
          </div>
        </div>
    </div>
  </div>
</div>

</div> --}}
