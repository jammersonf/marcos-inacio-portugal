<section class="p-0 section full-height-sm" id="servicos">
    <div class="container-fluid">
        <div class="content-title anim-up ">
            @lang('messages.services.title')
            
            <p style="max-width: 450px; font-size: 16px" class="pt-4">
                {{$desc->desc}}
            </p>
        </div>
        <div class="scrolling-wrapper anim-up" id="sobre-wrapper">
            @foreach($services as $service)
                <div  class="card card-servicos mx-3"> {{-- position:relative; top: 0; --}}
                    <a href="" data-toggle="modal" data-target="#mi{{$service->id}}" class="card-body">
                        <h3 style="white-space: pre-wrap; " class="card-title p-3">{{$service->name}}</h3>
                        <p style="white-space: pre-wrap; " class="d-none d-md-block card-text px-3">{{$service->short}}</p>
                        <p style="white-space: pre-wrap; max-height: 150px; overflow: auto;" class="d-md-none  card-text px-3">{{$service->short}}</p>
                        <a style="position:absolute; bottom:0;" class="btn-orange p-3" href="" data-toggle="modal"
                        data-target="#mi{{$service->id}}">@lang('messages.services.footer')</a>
                    </a>
                </div>
            @endforeach
        </div>
        {{-- <div class="d-none d-md-block">
            <div class=" d-flex justify-content-end">
                <a href="" class="btn-light mx-2" id="left-button">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                
                <a href="" class="btn-light mx-2" id="right-button">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div> --}}

    </div>
</section>
@section('modal')

    @foreach($services as $service)
    <div class="modal  fade" id="mi{{$service->id}}" tabindex="-1" role="dialog" aria-labelledby="{{$service->id}}"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header px-md-5 mx-2 pt-5">
                    <h1 style="max-width: 80%; " class="modal-title" id="{{$service->id}}">{{$service->name}}</h1>
                    <button type="button" class="close p-0" data-dismiss="modal" aria-label="Close">
                        <i class="icon icon-x-exit icon-2x p-4 "></i>
                    </button>
                </div>
                <div class="modal-body p-md-5 m-2">
                    {!! $service->text !!}  <br>
                    <a style="cursor: pointer;" class="btn btn-primary btn-lg mt-5 p-4 moveToContato"  data-dismiss="modal">
                        <strong>@lang('messages.about.contact')</strong>
                    </a>
                </div>
            </div>
        </div>
    </div>
    @endforeach
@endsection
