        <aside class="aside">
      
          <form action="#">
            <div class="box-search">
              <input type="text" name="q" value="{{$query}}" placeholder="Buscar" autofocus>
              <button><i class="fa fa-search" aria-hidden="true"></i></button>
            </div>
          </form>
      
          <div class="box">
            <h4 class="title small font-weight-bold">@lang('messages.news.cat')</h4>
            <div class="tags">
              <a href="{{route('noticias')}}" @if($cat == '') class="active" @endif> Tudo </a>
              @foreach($cats as $category)
                <a href="?cat={{$category->id}}" @if($cat == $category->id) class="active" @endif>{{$category->name}}</a>
              @endforeach
      
            </div>
      
            <h4 class="title small font-weight-bold">@lang('messages.news.read')</h4>
      
            <ul class="list-lidas">
            @foreach($reads as $post)
              @include('components.cards._postSmall')
            @endforeach
            </ul>
      
          </div>
        </aside>
      
