<section class="p-0 section mobile-fit" id="sobre">
    <div  class="container-fluid P-0">
        <ul class=" nav nav-pills mb-3 nav-sobre d-flex justify-content-center" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab"
                    aria-controls="pills-home" aria-selected="true">@lang('messages.about.head1')</a>
            </li>

            <li class="nav-item">
                <div class="horizontal-line"></div>
            </li>

            <li class="nav-item">
                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab"
                    aria-controls="pills-profile" aria-selected="false">@lang('messages.about.head2')</a>
            </li>
        </ul>

        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                {{-- <img class="d-md-none mobile-header" width="100%"
                    src="{{asset('assets_fronts/imgs/sobre-mobile-header.png')}}" alt=""> --}}
                    <div class="d-md-none" style="height: 209px; 
                                background-image: url('assets_fronts/imgs/sobre-nos-mobile.png');   
                                background-size: cover;
                                background-position: center;"></div>
                <div style="padding-top: 20px;" class="content-full mobile-header-offset">
                    <div class="center">

                        <div style="z-index: 0;" class="small-dots">
                        </div>
                    </div>

                    <div  class="row m-0">
                        <div class="col-md-5">

                            <div class="sobre-nos-title anim-up">
                                <h1 style="position: relative; z-index: 100; padding: 0px; min-width: 500px;" class="d-none d-md-block display-1">
                                    {!! $about->title !!}
                                </h1>
                                <h1 style="position: relative; z-index: 100;" class="d-md-none text-orange text-center">
                                    {!! $about->title !!}
                                </h1>

                            </div>
                            <div style="max-width: 450px" class="sobre-nos-text-box anim-left">

                                <p class="sobre-nos-text sobre-text-top"  >
                                    <i>{!! $about->subtitle !!}</i>
                                </p>
                                <p class="sobre-nos-text sobre-text-bot" >
                                    {!! $about->desc !!}
                                </p>
                            </div>

                            <div class="sobre-nos-btn  "> {{-- d-flex justify-content-between --}}
                                <div class="row m-1">
                                    <div class="col-md-6 p-3 p-sm-0">

                                        <a style="cursor: pointer; font-size: 12px;" class="btn btn-primary btn-lg btn-block anim-left p-4 moveToContato">
                                            <strong>@lang('messages.about.contact')</strong>
                                        </a>
                                    </div>
                                    <div class="col-md-6 p-3 p-sm-0">

                                        <a href="https://www.google.com.br/maps/place/Av.+da+Liberdade+110+409,+1250-096+Lisboa,+Portugal/@38.7188865,-9.145958,17z/data=!3m1!4b1!4m5!3m4!1s0xd1933823b357643:0xa473a18eb9cfd2c1!8m2!3d38.7188823!4d-9.1437693?hl=pt-BR&shorturl=1" 
                                            target="blank" class="btn btn-orange btn-block btn-lg anim-left p-4 mt-0">
                                            <strong style="white-space: nowrap; font-size: 11px;">@lang('messages.about.locate')</strong>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>
                        
                        <div style="height: calc(100vh - 200px);
                        background-image: url({{url('/storage/about/'.$about->img)}});  
                        background-size: cover;
                        background-position: center;
                        " class="col-md-6 col-lg-5 d-none d-md-block anim-right">
                            <div class="d-none d-md-block">
                                    {{-- <img class=" sobre-img"
                                    src="{{asset('assets_fronts/imgs/sobre-nos.png')}}" alt=""> --}}
                                </div>
                            </div>
                        <div class="d-none d-lg-block col-md-2 col-btn-profissionais anim-right">
                            <div class="d-none d-md-block ">

                                <button style="line-height: 0px;" class="btn-nossos-pro text-start d-none d-md-block btn btn-lg  profissionaisOutSide d-flex justify-content-between">
                                    <strong style="line-height: 0px;" >@lang('messages.about.prof')</strong> 
                                    <i class="text-orange icon icon-next-active  p-3 "></i>
                                </button>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
            <div style="height: calc(100vh - 170px); " class="tab-pane fade " id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                <div style="padding-top: 20px; height: 100%;position: relative;" class="content-full ">
                <div style="padding-top: 20px;" class=" d-flex justify-content-start">
                    <div class="row">
                        <div class="col-md-6 anim-down profissionals-title">
                            @lang('messages.about.profTitle')
                        </div>
                        <div class="col-md-6 anim-down">
                            <p style="max-width: 800px" class="pl-4 ">
                                {{$teamDesc->text}}
                            </p>
                        </div>
                    </div>
                </div>
                <div style="width: 100%;"  class="row">
                    <div class="col-md-10 anim-down">
                        <hr class="my-3 py-1">
                    </div>
                    <div class="col-md-2 anim-down">

                        <div class="d-none d-lg-block ">
                            <div class=" d-flex justify-content-end mb-3">
                                <button style="box-shadow: none;" class="btn btn-outline-primary mx-2 p-0" id="left-button-team">
                                    <i class="icon icon-back-active  icon-round "></i>
                                </button>
                                <button style="box-shadow: none;" class="btn btn-outline-primary mx-2 p-0" id="right-button-team">
                                    <i class="icon icon-next-active  icon-round "></i>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>

                <div style="padding: 0px" class="scrolling-wrapper anim-right" id="team-wrapper">
                    {{-- @foreach($team as $data) --}}
                    @for($i = 0; $i < count($team); $i++)
                    <a style="
                    " data-toggle="modal" data-target="#tm{{$team[$i]->id}}" class="project">
                        <figure class="img-responsive">
                            @if($i < 2)
                                <div style="
                                background-image: url('{{url('/storage/team/'.$team[$i]->image)}}');
                                background-size: cover;
                                background-position: center;
                                " class="card-profissionals-bg"></div>
                                <span class="actions pl-5">
                                    <h3 style="white-space: normal; font-size: 26px; width: 100px" class="text-white">{{$team[$i]->name}}</h3>
                                    <p class="text-white"><small>{{$team[$i]->job}}</small></p>
                                </span>
                                    
                            @else
                                <div style="
                                background-image: url('{{url('/storage/team/'.$team[$i]->image)}}');
                                background-size: cover;
                                background-position: center;
                                " class="card-profissionals-bg-short"></div>
                                <span class="actions pl-5 ">
                                    <h3 style="white-space: normal; font-size: 26px; width: 100px; padding-top: 17%" class="text-white">{{$team[$i]->name}}</h3>
                                    <p class="text-white"><small>{{$team[$i]->job}}</small></p>
                                </span>
                                    
                            @endif
                            {{-- <img height="300px" width="auto" src="{{url('/storage/team/'.$team[$i]->image)}}"> --}}
                        </figure>
                    </a>
                    @endfor
                    {{-- @endforeach --}}
                </div>

                <button style="position: absolute; bottom: 0; left: 0"
                    class="text-start d-none d-md-block btn float-left btn-lg sobreNosOutSide px-4 ml-5 anim-left">
                    <i class="text-orange icon icon-back-active  px-3 "></i><strong>@lang('messages.about.head1')</strong>
                </button>
            </div>
            </div>
        </div>


    </div>
</section>
@section('modal-sobre')
{{-- @foreach($team as $data) --}}
@for($i = 0; $i < count($team); $i++)
<div class="modal fade modal-team" id="tm{{$team[$i]->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-full" role="document">
        <div class="modal-content modal-content-team">
            <div class="modal-body p-0">
                <button type="button" class=" modal-previous btn btn-outline-light p-0 m-2" data-dismiss="modal"
                    aria-label="Close">
                    <i class="icon icon-back-active  icon-round "></i>
                    <span class="sr-only">Previous</span>
                </button>
                <ul class=" nav nav-pills  nav-sobre d-flex justify-content-center" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <button data-dismiss="modal" class="nav-link sobreNosOutSide  btn">@lang('messages.about.head1')</button>
                    </li>

                    <li class="nav-item ">
                        <div class="horizontal-line"></div>
                    </li>

                    <li class="nav-item">
                        <button class="btn nav-link active" data-dismiss="modal">@lang('messages.about.head2')</button>
                    </li>
                </ul>

                <div class="row img-cover-profissional">
                    <div style="background-image: url('{{url('/storage/team/'.$team[$i]->image)}}');
                    background-size: cover;
                    background-position: center;
                    height: calc(100vh - 92px);" class="col-lg-6 ">
                        {{-- <img class="img-team" src="{{url('/storage/team/'.$data->image)}}"> --}}
                    </div>
                    <div class="col-lg-6">
                        <div class="p-5">
                            <h2>{{$team[$i]->name}}</h2>
                            <p><small>{{$team[$i]->job}}</small></p>
                            {!! $team[$i]->text !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endfor
@endsection

{{-- @endforeach --}}
