@extends('components.layouts.default')

@include('popup')
@section('content')
    <div id="fullpage">
        @include('components.home')
        @include('components.servicos')
        @include('components.numeros')
        @include('components.sobre')
        @include('components.noticias')
        @include('components.contato')
    </div>
    @yield('modal')
    @yield('modal-sobre')
@endsection

@section('scripts')
    @yield('numeros-scripts')
@endsection


