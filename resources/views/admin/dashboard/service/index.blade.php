@extends('admin.layouts.default')

@section('content')

<section class="section">
  <div class="section-header d-flex justify-content-between">
    <h1>Serviços</h1>
    <a href="{{ route('adm.service.create') }}" class="btn btn-success">Cadastrar</a>
  </div>

  <div class="section-body">
    <div class="row">
      <div class="col-12 col-md-12 col-lg-12">
        <div class="card dashboard-funil-full">
          <table class="table table-responsive">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Serviço</th>
                <th scope="col">Short</th>
                <th scope="col">Lang</th>
                <th scope="col">Opções</th>
              </tr>
            </thead>
            <tbody>
              @foreach($data as $service)
              <tr>
                <th scope="row">{{$service->id}}</th>
                <td>{{$service->name}}</td>
                <td>{{$service->short}}</td>
                <td>{{$service->lang}}</td>
                <td>
                  <a href="{{ route('adm.service.show', $service->id) }}">Editar</a> | 
                  <a href="{{ route('adm.service.delete', $service->id)}} " onclick="return confirm('Deseja mesmo apagar?')">Apagar</a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

</section>


@endsection
