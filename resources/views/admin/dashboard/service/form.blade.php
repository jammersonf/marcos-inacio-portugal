@extends('admin.layouts.default')

@section('content')

<section class="section">
  <div class="section-header">
    <h1>Serviços</h1>
  </div>

  <div class="section-body">
    <div class="row">
      <div class="col-12 col-md-12 col-lg-12">
        <div class="card dashboard-funil-full">
          <div class="card-header">
            <h4>
              <i class="far fa-star lga"></i> Cadastrar
            </h4>
          </div>
          <div class="card-body row">
            @if(isset($service))
                {!! Form::model($service, ['route' => ['adm.service.update', $service->id]]) !!}
            @else
                {!! Form::open(['route' => 'adm.service.store']) !!}
            @endif
              Serviço: {!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
              <br>
              Resumo: {!! Form::text('short', null, ['class' => 'form-control', 'required']) !!}
              <br>
              Lang: {!! Form::select('lang', ['pt' => 'PT', 'en' => 'EN'], null, ['class' => 'form-control', 'required']) !!}
              <br>
              Descrição: {!! Form::textarea('text', null, ['class' => 'form-control ckeditor', 'required']) !!}
              <br>
              <a href="{{url()->previous()}}" class="btn btn-primary">Voltar</a>
              {!! Form::submit('Enviar', ['class' => 'btn btn-success']) !!}
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>

@endsection

@section('scripts')
  <script src="http://vip.ufcode.com.br/ckeditor/ckeditor.js"></script>
@endsection
