@extends('admin.layouts.default')

@section('content')

<section class="section">
  <div class="section-header">
    <h1>Descrição</h1>
  </div>

  <div class="section-body">
    <div class="row">
      <div class="col-12 col-md-12 col-lg-12">
        <div class="card dashboard-funil-full">
          <div class="card-header">
            <h4>
              <i class="far fa-star lga"></i> Título
            </h4>
          </div>
          <div class="card-body row">
              {!! Form::open(['route' => 'adm.service.updateDesc']) !!}
              PT: {!! Form::textarea('pt', $pt->desc, ['class' => 'form-control', 'required']) !!}
              EN: {!! Form::textarea('en', $en->desc, ['class' => 'form-control', 'required']) !!}
              <br>
              <a href="{{url()->previous()}}" class="btn btn-primary">Voltar</a>
              {!! Form::submit('Enviar', ['class' => 'btn btn-success']) !!}

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>


@endsection
