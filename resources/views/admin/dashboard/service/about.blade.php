@extends('admin.layouts.default')

@section('content')

<section class="section">
  <div class="section-header">
    <h1>Sobre</h1>
  </div>

  <div class="section-body">
    <div class="row">
      <div class="col-6 col-md-6 col-lg-6">
        <div class="card dashboard-funil-full">
          <div class="card-header">
            <h4>
              <i class="far fa-star lga"></i> PT
            </h4>
          </div>
          <div class="card-body row">
              {!! Form::open(['route' => 'adm.service.updateAbout', 'files' => true]) !!}
              {!! Form::hidden('lang', 'pt') !!}
              {!! Form::label('t', 'Título') !!}
              {!! Form::text('title', $pt->title, ['class' => 'form-control']) !!}
              <br>
              {!! Form::label('t', 'Subtítulo') !!}
              {!! Form::textarea('subtitle', $pt->subtitle, ['class' => 'form-control']) !!}
              <br>
              {!! Form::label('t', 'Descrição') !!}
              {!! Form::textarea('desc', $pt->desc, ['class' => 'form-control']) !!}
              <br>
              {!! Form::label('t', 'Imagem') !!}
              {!! Form::file('img') !!}
              <br>
              <a href="{{url()->previous()}}" class="btn btn-primary">Voltar</a>
              {!! Form::submit('Enviar', ['class' => 'btn btn-success']) !!}
            </form>
          </div>
        </div>
      </div>
      <div class="col-6 col-md-6 col-lg-6">
        <div class="card dashboard-funil-full">
          <div class="card-header">
            <h4>
              <i class="far fa-star lga"></i> EN
            </h4>
          </div>
          <div class="card-body row">
              {!! Form::open(['route' => 'adm.service.updateAbout', 'files' => true]) !!}
              {!! Form::hidden('lang', 'en') !!}
              {!! Form::label('t', 'Título') !!}
              {!! Form::text('title', $en->title, ['class' => 'form-control']) !!}
              <br>
              {!! Form::label('t', 'Subtítulo') !!}
              {!! Form::textarea('subtitle', $en->subtitle, ['class' => 'form-control']) !!}
              <br>
              {!! Form::label('t', 'Descrição') !!}
              {!! Form::textarea('desc', $en->desc, ['class' => 'form-control']) !!}
              <br>
              {!! Form::label('t', 'Imagem') !!}
              {!! Form::file('img') !!}
              <br>
              {!! Form::submit('Enviar', ['class' => 'btn btn-success']) !!}
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>


@endsection
