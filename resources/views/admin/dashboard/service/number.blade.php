@extends('admin.layouts.default')

@section('content')

<section class="section">
  <div class="section-header">
    <h1>Nossos Números</h1>
  </div>

  <div class="section-body">
    <div class="row">
      <div class="col-6 col-md-6 col-lg-6">
        <div class="card dashboard-funil-full">
          <div class="card-header">
            <h4>
              <i class="far fa-star lga"></i> PT
            </h4>
          </div>
          <div class="card-body row">
              {!! Form::open(['route' => 'adm.service.updateNumber']) !!}
              {!! Form::hidden('lang', 'pt') !!}
              {!! Form::label('t', 'Seção 1') !!}
              {!! Form::text('t1', $pt->t1, ['class' => 'form-control']) !!}
              {!! Form::number('n1', $pt->n1, ['class' => 'form-control']) !!}
              <br>
              {!! Form::label('t', 'Seção 2') !!}
              {!! Form::text('t2', $pt->t2, ['class' => 'form-control']) !!}
              {!! Form::number('n2', $pt->n2, ['class' => 'form-control']) !!}
              <br>
              {!! Form::label('t', 'Seção 3') !!}
              {!! Form::text('t3', $pt->t3, ['class' => 'form-control']) !!}
              {!! Form::number('n3', $pt->n3, ['class' => 'form-control']) !!}
              <br>
              {!! Form::label('t', 'Seção 4') !!}
              {!! Form::text('t4', $pt->t4, ['class' => 'form-control']) !!}
              {!! Form::number('n4', $pt->n4, ['class' => 'form-control']) !!}
              <br>
              {!! Form::label('t', 'Seção 5') !!}
              {!! Form::text('t5', $pt->t5, ['class' => 'form-control']) !!}
              {!! Form::number('n5', $pt->n5, ['class' => 'form-control']) !!}
              <br>
              {!! Form::label('t', 'Seção 6') !!}
              {!! Form::text('t6', $pt->t6, ['class' => 'form-control']) !!}
              {!! Form::number('n6', $pt->n6, ['class' => 'form-control']) !!}
              <br>
              <a href="{{url()->previous()}}" class="btn btn-primary">Voltar</a>
              {!! Form::submit('Enviar', ['class' => 'btn btn-success']) !!}

            </form>
          </div>
        </div>
      </div>
      <div class="col-6 col-md-6 col-lg-6">
        <div class="card dashboard-funil-full">
          <div class="card-header">
            <h4>
              <i class="far fa-star lga"></i> EN
            </h4>
          </div>
          <div class="card-body row">
              {!! Form::open(['route' => 'adm.service.updateNumber']) !!}
              {!! Form::hidden('lang', 'en') !!}
              {!! Form::label('t', 'Seção 1') !!}
              {!! Form::text('t1', $en->t1, ['class' => 'form-control']) !!}
              {!! Form::text('n1', $en->n1, ['class' => 'form-control']) !!}
              <br>
              {!! Form::label('t', 'Seção 2') !!}
              {!! Form::text('t2', $en->t2, ['class' => 'form-control']) !!}
              {!! Form::text('n2', $en->n2, ['class' => 'form-control']) !!}
              <br>
              {!! Form::label('t', 'Seção 3') !!}
              {!! Form::text('t3', $en->t3, ['class' => 'form-control']) !!}
              {!! Form::text('n3', $en->n3, ['class' => 'form-control']) !!}
              <br>
              {!! Form::label('t', 'Seção 4') !!}
              {!! Form::text('t4', $en->t4, ['class' => 'form-control']) !!}
              {!! Form::text('n4', $en->n4, ['class' => 'form-control']) !!}
              <br>
              {!! Form::label('t', 'Seção 5') !!}
              {!! Form::text('t5', $en->t5, ['class' => 'form-control']) !!}
              {!! Form::text('n5', $en->n5, ['class' => 'form-control']) !!}
              <br>
              {!! Form::label('t', 'Seção 6') !!}
              {!! Form::text('t6', $en->t6, ['class' => 'form-control']) !!}
              {!! Form::text('n6', $en->n6, ['class' => 'form-control']) !!}
              <br>
              {!! Form::submit('Enviar', ['class' => 'btn btn-success']) !!}

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>


@endsection
