@extends('admin.layouts.default')

@section('content')

<section class="section">
  <div class="section-header">
    <h1>Serviços</h1>
  </div>

  <div class="section-body">
    <div class="row">
      <div class="col-12 col-md-12 col-lg-12">
        <div class="card dashboard-funil">
          <div class="card-header">
            <h4>
              <i class="far fa-star lga"></i> Cadastrar
            </h4>
          </div>
          <div class="card-body row">
            @if(isset($testimonial))
                {!! Form::model($testimonial, ['route' => ['adm.testimonial.update', $testimonial->id]]) !!}
            @else
                {!! Form::open(['route' => 'adm.testimonial.store']) !!}
            @endif
              Nome: {!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
              <br>
              Lang: {!! Form::select('lang', ['pt' => 'PT', 'en' => 'EN'], null, ['class' => 'form-control', 'required']) !!}
              <br>
              Depoimento: {!! Form::textarea('text', null, ['class' => 'form-control', 'required']) !!}
              <br>
              <a href="{{url()->previous()}}" class="btn btn-primary">Voltar</a>
              {!! Form::submit('Enviar', ['class' => 'btn btn-success']) !!}
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>


@endsection
