@extends('admin.layouts.default')

@section('content')

<section class="section">
  <div class="section-header d-flex justify-content-between">
    <h1>Depoimentos</h1>
    <a href="{{ route('adm.testimonial.create') }}" class="btn btn-success">Cadastrar</a>
  </div>

  <div class="section-body">
    <div class="row">
      <div class="col-12 col-md-12 col-lg-12">
        <div class="card dashboard-funil">
          <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nome</th>
                <th scope="col">Lang</th>
                <th scope="col">Opções</th>
              </tr>
            </thead>
            <tbody>
              @foreach($data as $testimonial)
              <tr>
                <th scope="row">{{$testimonial->id}}</th>
                <td>{{$testimonial->name}}</td>
                <td>{{$testimonial->lang}}</td>
                <td>
                  <a href="{{ route('adm.testimonial.show', $testimonial->id) }}">Editar</a> | 
                  <a href="{{ route('adm.testimonial.delete', $testimonial->id)}} " onclick="return confirm('Deseja mesmo apagar?')">Apagar</a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

</section>


@endsection
