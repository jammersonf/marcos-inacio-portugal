@extends('admin.layouts.default')

@section('content')

<section class="section">
  <div class="section-header">
    <h1>Home</h1>
  </div>

  <div class="section-body">
    <div class="row">
      <div class="col-12 col-md-12 col-lg-12">
        <div class="card dashboard-funil-full">
          <div class="card-header">
            <h4>
              <i class="far fa-star lga"></i> Cadastrar
            </h4>
          </div>
          <div class="card-body row">
            @if(isset($home))
                {!! Form::model($home, ['route' => ['adm.home.update', $home->id], 'files' => true]) !!}
            @else
                {!! Form::open(['route' => 'adm.home.store', 'files' => true]) !!}
            @endif
              Imagem: {!! Form::file('image') !!}
              <br>
              Titulo: {!! Form::text('title', null, ['class' => 'form-control', 'required']) !!}
              <br>
              Sub titulo: {!! Form::text('subtitle', null, ['class' => 'form-control', 'required']) !!}
              <br>
              Título texto apoio: {!! Form::text('title_support', null, ['class' => 'form-control', 'required']) !!}
              <br>
              Texto apoio: {!! Form::text('support', null, ['class' => 'form-control', 'required']) !!}
              <br>
              Lang: {!! Form::select('lang', ['pt' => 'PT', 'en' => 'EN'], null, ['class' => 'form-control', 'required']) !!}
              <br>
              <a href="{{url()->previous()}}" class="btn btn-primary">Voltar</a>
              {!! Form::submit('Enviar', ['class' => 'btn btn-success']) !!}
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>

@endsection
