@extends('admin.layouts.default')

@section('content')

<section class="section">
  <div class="section-header d-flex justify-content-between">
    <h1>Home</h1>
    <a href="{{ route('adm.home.create') }}" class="btn btn-success">Cadastrar</a>
  </div>

  <div class="section-body">
    <div class="row">
      <div class="col-12 col-md-12 col-lg-12">
        <div class="card dashboard-funil-full">
          <table class="table table-responsive">  
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Título</th>
                <th scope="col">Subtítulo</th>
                <th scope="col">Lang</th>
                <th scope="col">Opções</th>
              </tr>
            </thead>
            <tbody>
              @foreach($data as $home)
              <tr>
                <th scope="row">{{$home->id}}</th>
                <td>{{$home->title}}</td>
                <td>{{$home->subtitle}}</td>
                <td>{{$home->lang}}</td>
                <td>
                  <a href="{{ route('adm.home.show', $home->id) }}">Editar</a> | 
                  <a href="{{ route('adm.home.delete', $home->id)}} " onclick="return confirm('Deseja mesmo apagar?')">Apagar</a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

</section>


@endsection
