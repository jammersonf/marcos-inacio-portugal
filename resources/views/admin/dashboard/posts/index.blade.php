@extends('admin.layouts.default')

@section('content')

<section class="section">
  <div class="section-header d-flex justify-content-between">
    <h1>Posts</h1>
    <a href="{{ route('adm.post.create') }}" class="btn btn-success">Cadastrar</a>
  </div>
  <div class="section-header d-flex justify-content-between">
    <h1>Pesquisar</h1>
    <br>
    <form action="#">
      <div class="box-search">
        <input type="text" name="q" value="{{$query}}" placeholder="Buscar" autofocus>
        <button><i class="fa fa-search" aria-hidden="true"></i></button>
      </div>
    </form>
  </div>
  <div class="section-body">
    <div class="row">
      <div class="col-12 col-md-12 col-lg-12">
        <div class="card dashboard-funil-full">
          <table class="table table-responsive">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Título</th>
                <th scope="col">Categoria</th>
                <th scope="col">Visualizações</th>
                <th scope="col">Lang</th>
                <th scope="col">Opções</th>
              </tr>
            </thead>
            <tbody>
              @foreach($data as $post)
              <tr>
                <th scope="row">{{$post->id}}</th>
                <td>{{$post->title}}</td>
                <td>{{$post->category_name}}</td>
                <td>{{$post->read}}</td>
                <td>{{$post->lang}}</td>
                <td>
                  <a href="{{ route('adm.post.show', $post->id) }}">Editar</a> | 
                  <a href="{{ route('adm.post.delete', $post->id)}} " onclick="return confirm('Deseja mesmo apagar?')">Apagar</a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

</section>


@endsection
