@extends('admin.layouts.default')

@section('content')

<section class="section">
  <div class="section-header">
    <h1>Posts</h1>
  </div>

  <div class="section-body">
    <div class="row">
      <div class="col-12 col-md-12 col-lg-12">
        <div class="card dashboard-funil-full">
          <div class="card-header">
            <h4>
              <i class="far fa-star lga"></i> Cadastrar
            </h4>
          </div>
          <div class="card-body row">
            @if(isset($post))
                {!! Form::model($post, ['route' => ['adm.post.update', $post->id], 'files' => true]) !!}
            @else
                {!! Form::open(['route' => 'adm.post.store', 'files' => true]) !!}
            @endif
              Título (50 caracteres): {!! Form::text('title', null, ['class' => 'form-control', 'maxlength' => 50, 'required']) !!}
              <br>
              Sub título: {!! Form::text('subtitle', null, ['class' => 'form-control', 'required']) !!}
              <br>
              Categoria: {!! Form::select('category_id', $categories, null, ['class' => 'form-control', 'required']) !!}
              <br>
              @if(!isset($post))
                Capa: {!! Form::file('image') !!}
                <br>
              @endif
              Lang: {!! Form::select('lang', ['pt' => 'PT', 'en' => 'EN'], null, ['class' => 'form-control', 'required']) !!}
              <br>
              Texto: {!! Form::textarea('text', null, ['class' => 'form-control ckeditor', 'required']) !!}
              <br>
              <a href="{{url()->previous()}}" class="btn btn-primary">Voltar</a>
              {!! Form::submit('Enviar', ['class' => 'btn btn-success']) !!}
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>

@endsection

@section('scripts')
  <script src="{{ url('ckeditor/ckeditor.js') }}"></script>
@endsection
