@extends('admin.layouts.default')

@section('content')

<section class="section">
  <div class="section-header d-flex justify-content-between">
    <h1>Profissionais</h1>
    <a href="{{ route('adm.team.create') }}" class="btn btn-success">Cadastrar</a>
  </div>

  <div class="section-body">
    <div class="row">
      <div class="col-12 col-md-12 col-lg-12">
        <div class="card dashboard-funil-full">
          <table class="table">
            <thead class="thead-light">
              <tr>
                <th>#</th>
                <th>Nome</th>
                <th>Lang</th>
                <th>Opções</th>
              </tr>
            </thead>
            <tbody>
              @foreach($data as $team)
              <tr>
                <td>{{$team->id}}</th>
                <td>{{$team->name}}</td>
                <td>{{$team->lang}}</td>
                <td>
                  <a href="{{ route('adm.team.show', $team->id) }}">Editar</a> | 
                  <a href="{{ route('adm.team.delete', $team->id)}} " onclick="return confirm('Deseja mesmo apagar?')">Apagar</a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

</section>


@endsection
