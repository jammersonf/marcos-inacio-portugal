@extends('admin.layouts.default')

@section('content')

<section class="section">
  <div class="section-header">
    <h1>Profissionais</h1>
  </div>

  <div class="section-body">
    <div class="row">
      <div class="col-12 col-md-12 col-lg-12">
        <div class="card dashboard-funil-full">
          <div class="card-header">
            <h4>
              <i class="far fa-star lga"></i> Cadastrar
            </h4>
          </div>
          <div class="card-body row">
            @if(isset($team))
                {!! Form::model($team, ['route' => ['adm.team.update', $team->id], 'files' => true]) !!}
            @else
                {!! Form::open(['route' => 'adm.team.store', 'files' => true]) !!}
            @endif
              Nome: {!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
              <br>
              Ocupação: {!! Form::text('job', null, ['class' => 'form-control', 'required']) !!}
              <br>
              Imagem: {!! Form::file('image') !!}
              <br>
              Lang: {!! Form::select('lang', ['pt' => 'PT', 'en' => 'EN'], null, ['class' => 'form-control', 'required']) !!}
              <br>
              Texto: {!! Form::textarea('text', null, ['class' => 'form-control ckeditor', 'required']) !!}
              <br>
              <a href="{{url()->previous()}}" class="btn btn-primary">Voltar</a>
              {!! Form::submit('Enviar', ['class' => 'btn btn-success']) !!}
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>

@endsection

@section('scripts')
  <script src="http://vip.ufcode.com.br/ckeditor/ckeditor.js"></script>
@endsection
