@extends('admin.layouts.default')

@section('content')

<section class="section">
  <div class="section-header">
    <h1>Contatos</h1>
  </div>

  <div class="section-body">
    <div class="row">
      <div class="col-12 col-md-12 col-lg-12">
        <div class="card dashboard-funil-full">
          <table class="table table-responsive">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nome</th>
                <th scope="col">Email</th>
                <th scope="col">Telefone</th>
                <th scope="col">Lang</th>
              </tr>
            </thead>
            <tbody>
              @foreach($data as $contact)
              <tr>
                <th scope="row">{{$contact->id}}</th>
                <td>{{$contact->name}}</td>
                <td>{{$contact->email}}</td>
                <td>{{$contact->phone}}</td>
                <td>{{$contact->lang}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

</section>


@endsection
