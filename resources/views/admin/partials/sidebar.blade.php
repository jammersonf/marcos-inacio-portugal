<div class="main-sidebar sidebar-style-2">
  <aside id="sidebar-wrapper">
    <div class="sidebar-brand mb-2">
      <a href="{{ route('adm.panel') }}"><img src="{{ asset('/assets/img/brand.svg') }}" width="50"></a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
      <a href="{{ route('adm.panel') }}"><img src="{{ asset('/assets/img/brand.svg') }}" width="50"></a>
    </div>
    <ul class="sidebar-menu">
      <li class="menu-header">Módulos</li>
      <li>
        <a href="{{route('adm.home.index')}}">
            <i class="icon icon-home icon-2x"></i>
        <span>Home</span>
      </a>
      </li>
      <li class="dropdown ">
        <a href="#" class="nav-link has-dropdown">
          <i class="icon icon-servicos icon-2x"></i>
          <span>Serviços</span>
        </a>
        <ul class="dropdown-menu" style="display: none;">
          <li>
            <a class="nav-link" href="{{route('adm.service.index')}}" title="Serviços">Serviços</a>
          </li>
          <li>
            <a class="nav-link" href="{{route('adm.service.createDesc')}}" title="Categorias">Descrição</a>
          </li>
          <li>
            <a class="nav-link" href="{{route('adm.service.createNumber')}}" title="Categorias">Números</a>
          </li>
        </ul>
      </li>
      <li class="dropdown ">
        <a href="#" class="nav-link has-dropdown">
          <i class="icon icon-servicos icon-2x"></i>
          <span>Sobre</span>
        </a>
        <ul class="dropdown-menu" style="display: none;">
          <li>
            <a class="nav-link" href="{{route('adm.service.createAbout')}}" title="Sobre">Sobre</a>
          </li>
          <li>
            <a class="nav-link" href="{{route('adm.service.createNumber')}}" title="Categorias">Números</a>
          </li>
        </ul>
      </li>
      <li>
        <a href="{{route('adm.testimonial.index')}}">
            <i class="icon icon-servicos icon-2x "></i>
        <span>Depoimentos</span>
      </a>
      </li>
      <li class="dropdown ">
        <a href="#" class="nav-link has-dropdown">
          <i class="icon icon-servicos icon-2x"></i>
          <span>Profissionais</span>
        </a>
        <ul class="dropdown-menu" style="display: none;">
          <li>
            <a class="nav-link" href="{{route('adm.team.index')}}" title="Serviços">Profissionais</a>
          </li>
          <li>
            <a class="nav-link" href="{{route('adm.team.createDesc')}}" title="Categorias">Descrição</a>
          </li>
        </ul>
      </li>
      <li class="dropdown ">
        <a href="#" class="nav-link has-dropdown">
          <i class="icon icon-noticias lga"></i>
          <span>Postagens</span>
        </a>
        <ul class="dropdown-menu" style="display: none;">
          <li>
            <a class="nav-link" href="{{route('adm.post.index')}}" title="Postagens">Postagens</a>
          </li>
          <li>
            <a class="nav-link" href="{{route('adm.post.category.index')}}" title="Categorias">Categorias</a>
          </li>
        </ul>
      </li>
      <li>
        <a href="{{route('adm.contact')}}">
            <i class="icon icon-contato icon-2x "></i>
        <span>Contatos</span>
      </a>
      </li>
    </ul>
  </aside>
</div>
