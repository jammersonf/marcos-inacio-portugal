<?php

return [
    'menu1'  => 'HOME',
    'menu2'  => 'SERVICES',
    'menu3'  => 'ABOUT',
    'menu4'  => 'NEWS',
    'menu5'  => 'CONTACT',
    'footer' => 'ACCESS THE BRAZILIAN PORTAL',
    'services' => [
        'title'  => '<h1 class="text-dark-2">Our</h1>
            <h1 class="text-orange">Services</h1>',
        'footer' => 'KNOW MORE'
    ],
    'btn-br' => '<a style="border-radius: 6px" href="?lang=pt" class="mx-1 px-4 py-3 btn btn-outline-secondary">PT</a>',
    'btn-en' => '<a style="border-radius: 6px" href="?lang=en" class="mx-1 px-4 py-3 btn btn-outline-secondary btn-active">EN</a>',
    'btn-side-br' => '<a style="border-radius: 6px" href="?lang=pt" class="mx-1 btn btn-outline-secondary">PT</a>',
    'btn-side-en' => '<a style="border-radius: 6px" href="?lang=en" class="mx-1 btn btn-outline-secondary active">EN</a>',
    'depo' => '<h1 class="text-orange">Testimony</h1>
                    <h1 class="text-white mb-5 pb-4">from our customers</h1>',
    'about' => [
        'head1' => 'ABOUT US',
        'head2' => 'PROFESSIONALS',
        'text1' => 'It has been working since 1990, working and evolving to serve its clients in the most diverse areas of law, always in a creative, safe, innovative, personalized and effective way.',
        'text2' => 'Headquartered in João Pessoa, PB, it has operating units spread across major cities in the Northeast, endowed with great structure and organized along the lines of professional excellence of the most reputable law firms in the country. The constant updating in line with the trends of modern law, has allowed the expansion of its services, increasingly recognized in the courts and the legal market.',
        'locate' => 'See location',
        'prof' => 'OUR PROFESSIONALS',
        'contact' => 'Get in Touch',
        'profTitle' => '<h1 class="text-dark-2">Our</h1>
                            <h1 class="text-orange">Professionals</h1>',
        'numbers' => '<h1 class="text-white">Our</h1>
            <h1 class="text-orange">Numbers</h1>',
    ],
    'news' => [
        'title' => '<h1 class="text-white">Follow <br> our contents</h1>',
        'title2' => '<h1>Our</h1>
          <h1 class="text-orange">contents</h1>',
        'title3' => '<h1>Articles</h1>
          <h1 class="text-orange">related</h1>',
        'footer' => 'All Posts',
        'cat' => 'Categories',
        'share' => 'Share',
        'search' => 'Search',
        'read' => 'More reads',
        'comment' => 'Comments'
    ],
    'contact' => [
        'title' => 'Let\'s  <br>
        <span class="text-orange">Talk</span>',
        'title2' => 'Our <br>
            <span class="text-orange">location</span>',
        'input1' => 'Name',
        'input2' => 'Email',
        'input3' => 'Phone',
        'button' => 'Send'
    ]
];
