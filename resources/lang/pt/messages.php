<?php

return [
    'menu1'  => 'HOME',
    'menu2'  => 'SERVIÇOS',
    'menu3'  => 'SOBRE',
    'menu4'  => 'NOTÍCIAS',
    'menu5'  => 'CONTATO',
    'footer' => 'Acesse o portal <br> Brasileiro',
    'services' => [
        'title'  => '<h1 class="text-dark-2">Nossos</h1>
            <h1 class="text-orange">Serviços</h1>',
        'footer' => 'Saiba mais'
    ],
    'btn-br' => '<a style="border-radius: 6px" href="?lang=pt" class="mx-1 px-4 py-3 btn btn-outline-secondary btn-active">PT</a>',
    'btn-en' => '<a style="border-radius: 6px" href="?lang=en" class="mx-1 px-4 py-3 btn btn-outline-secondary">EN</a>',
    'btn-side-br' => '<a style="border-radius: 6px" href="?lang=pt" class="mx-1 btn btn-outline-secondary active">PT</a>',
    'btn-side-en' => '<a style="border-radius: 6px" href="?lang=en" class="mx-1 btn btn-outline-secondary">EN</a>',
    'depo' => '<h1 class="text-orange">Depoimentos</h1>
                    <h1 class="text-white mb-5 pb-4">dos nossos clientes</h1>',
    'about' => [
        'head1' => 'SOBRE NÓS',
        'head2' => 'PROFISSIONAIS',
        'text1' => 'Atua desde 1990, trabalhando e evoluindo para bem servir a seus clientes, nas mais diversas áreas do Direito, sempre de forma criativa, segura , inovadora, personalizada e eficaz.',
        'text2' => 'Sediada em João Pessoa, PB, conta com unidades operacionais espalhadas nas principais cidades do Nordeste, dotadas de ótima estrutura e organizadas nos moldes de excelência profissional dos mais conceituados escritórios de advocacia do país. A atualização constante em sintonia com as tendências da moderna advocacia, tem permitido a ampliação dos seus serviços, cada vez mais reconhecidos nos Tribunais e no mercado jurídico.',
        'locate' => 'VER LOCALIZAÇÃO',
        'prof' => 'NOSSOS PROFISSIONAIS',
        'contact' => 'Entrar em contato',
        'profTitle' => '<h1 class="text-dark-2">Nossos</h1>
                            <h1 class="text-orange">Profissionais</h1>',
        'numbers' => '<h1 class="text-white">Nossos</h1>
            <h1 class="text-orange">Números</h1>',
    ],
    'news' => [
        'title' => '<h1 class="text-white">Acompanhe <br> nossos conteúdos</h1>',
        'title2' => '<h1>Nossos</h1>
          <h1 class="text-orange">conteúdos</h1>',
        'title3' => '<h1>Artigos</h1>
          <h1 class="text-orange">relacionados</h1>',
        'footer' => 'VER TODAS AS PUBLICAÇÕES',
        'cat' => 'Categorias',
        'share' => 'Compartilhe',
        'search' => 'Buscar',
        'read' => 'Mais lidas',
        'comment' => 'Comentários'
    ],
    'contact' => [
        'title' => 'Vamos <br>
        <span class="text-orange">Conversar</span>',
        'title2' => 'Nossa <br>
            <span class="text-orange">localização</span>',
        'input1' => 'Nome',
        'input2' => 'Insira seu e-mail',
        'input3' => 'Número de telefone',
        'button' => 'Enviar'
    ]
];
